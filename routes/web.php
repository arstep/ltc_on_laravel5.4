<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

// Site public ------------------------------------------------------------------------------------

Route::get('/', ['as' => 'index', 'uses' => 'Site\IndexController@index']);


Route::get('/about', ['uses' => 'Site\IndexController@about']);


Route::get('/reviews', ['uses' => 'Site\IndexController@reviews']);
Route::post('/comment', 'Site\CommentController@store');


Route::get('/article/{id}', ['uses' => 'Site\ArticlesController@getArticle']);
Route::get('/articles', ['uses' => 'Site\ArticlesController@getArticles']);


Route::get('/predictions/{typeOfSport}/{idPrediction}', [
                                                'uses' => 'Site\PredictionsController@singlePrediction',
                                                'as' => 'prediction'
                                                ]);
Route::get('/predictions/{typeOfSport}', ['uses' => 'Site\PredictionsController@typeOfSport']);
Route::get('/predictions', ['uses' => 'Site\PredictionsController@showAll']);


Route::get('/results/{type}/{id}', [
    'uses' => 'Site\ResultController@singlePrediction',
    'as' => 'result'
]);
Route::get('/results', ['uses' => 'Site\ResultsController@index']);


// Купить
Route::group(['prefix' => '/shop'], function (){

    Route::get('/', ['uses' => 'Site\ShopController@index']);
    Route::get('/subscribe/{id}', ['uses' => 'Site\ShopController@getSubscribe']);
});


// Корзина
Route::group(['prefix' => '/cart'], function (){

    Route::get('/', ['uses' => 'Site\CartController@index']);
    Route::get('/completion', ['uses' => 'Site\CartController@completion']);
    Route::get('/delete/{id}', ['uses' => 'Site\CartController@deleteGoods']);
    // Ajax запрос
    Route::post('/add', ['uses' => 'Site\CartController@addGoods']);

});


// Кабинет
Route::group(['prefix' => '/cabinet', 'middleware' => ['auth']], function (){

    Route::get('/main', ['uses' => 'Site\CabinetController@index']);
    Route::post('/userData', ['uses' => 'Site\CabinetController@userData']);
    Route::get('/getModal', ['uses' => 'Site\CabinetController@getModal']);
});


// Почта
Route::post('/mail', ['uses' => 'Site\MailController@send']);


// Аутентификация
Auth::routes();
//Route::get('/home', 'HomeController@index');
Route::get('/social/login_vk', 'Site\SocialController@login_vk');
Route::get('/social/callback_vk', 'Site\SocialController@callback_vk');

Route::get('/social/login_ok', 'Site\SocialController@login_ok');
Route::get('/social/callback_ok', 'Site\SocialController@callback_ok');

Route::post('/social/callback_ulogin', 'Site\SocialController@callback_ulogin');


//Административная часть ----------------------------------------------------------------------------------------

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

    Route::get('/', ['uses' => 'Admin\IndexController@index']);
    Route::resource('/articles', 'Admin\ArticlesController');
    Route::resource('/users', 'Admin\UserController');
    Route::resource('/predictions', 'Admin\PredictionsController');
});
















