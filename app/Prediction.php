<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prediction extends Model
{
    use SoftDeletes;

    // используется мягкое удаление
    protected $dates = ['deleted_at'];

    protected $fillable = [

        'commercial',
        'event_date',
        'type_of_sport',
        'region',
        'league',
        'team_one',
        'team_two',
        'code',
        'rate',
        'result',
    ];

}
