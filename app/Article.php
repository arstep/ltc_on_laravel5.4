<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    // Если используется мягкое удаление
    use SoftDeletes;

    protected $fillable = ['title', 'description', 'alias', 'text', 'img'];

    // Если используется мягкое удаление
    protected $dates = ['deleted_at'];


    public function articleimgs()
    {
        return $this->hasMany('App\Articleimg', 'article_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'article_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
