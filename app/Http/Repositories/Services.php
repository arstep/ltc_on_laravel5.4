<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 07.03.2017
 * Time: 21:39
 */

namespace App\Http\Repositories;
use Illuminate\Support\Facades\Auth;


abstract class Services
{

    // Информация о пользователе и его ролях в удобном виде
    // - отказался от использования -
/*    public static function getAuthUser()
    {
        $userModel = Auth::user();

        $roles = $userModel->roles;

        foreach ($roles as $role){

            $rolesArr[$role->role] = $role->title;
        }

        return $user = ['model' => $userModel, 'roles' => $rolesArr];
    }*/


    // Транслитерация для формирования поля alias для ЧПУ статей при добавлении статьи
    public static function transliterate($string)
    {

        $str = mb_strtolower($string, 'UTF-8');

        $letter_array = [
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ё" => "e",
            "ж" => "zh",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "kh",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "shch",
            "ы" => "y",
            "э" => "e",
            "ю" => "yu",
            "я" => "ya",
            "ъ" => "",
            "ь" => "",
        ];

        foreach ($letter_array as $kyr => $lat){

            $str = str_replace($kyr,$lat, $str);
        }

        $str = preg_replace('/(\s|[^A-Za-z0-9\-])+/','-', $str);

        $str = trim($str, '-');

        return $str;
    }
}