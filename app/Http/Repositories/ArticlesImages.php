<?php
/**
 * Предоставляет картинки для статей (max) и спиков статей (mini).
 * Проверяет их наличие на диске. Устанавливает картинки по умолчанию.
 */

namespace App\Http\Repositories;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait ArticlesImages
{

    // Добавит фотографии к каждой статье из списка
    protected function addArrayImages($paginates)
    {
        foreach ($paginates as $article) {

            $article = $this->addImage($article);
        }

        return $paginates;
    }

    // Добавит фотографии к статье
    protected function addImage($article)
    {
        $default_images = Config::get('settings.articles_default_img');

        if ( ! empty($article->img)) {

            $article->img = json_decode($article->img);

            file_exists(env('THEME') . '/img/articles/' . $article->img->max) ? false : $article->img->max = $default_images['max'];
            file_exists(env('THEME') . '/img/articles/' . $article->img->mini) ? false : $article->img->mini = $default_images['mini'];
        } else {

            // Если картинок нет - ставим по умолчанию
            // Для единообразия работы с изображениями в шаблонах, записываем картинки в виде свойств объекта
            $article->img = new \stdClass;
            $article->img->max = $default_images['max'];
            $article->img->mini = $default_images['mini'];
        }

        return $article;
    }


    // Автоматическая обработка загружаемого для статьи изображения с конвертацией в три файла и
    // формированием строки в формате JSON для сохранения названий файлов в БД
    protected function makeJsonImage($request)
    {
        // Берем объект изображения
        $image = $request->file('image');

        // Проверяем корректность загрузки изображения на сервер
        if ($image->isValid()){

            // Генерируем случайную строку для использования в качестве имени файла изображения
            $str = str_random(8);

            // Для формирования объекта json, создаем пустой объект стандартного класса рнр
            $obj = new \stdClass;

            $obj->mini = $str.'_mini.jpg';
            $obj->max = $str.'_max.jpg';
            $obj->path = $str.'.jpg';

            $img = Image::make($image);

            // Создаем "оргинал для хранения на сервере"
            $img->fit(Config::get('settings.image')['width'],
                Config::get('settings.image')['heigth'])
                ->save(public_path().'/'.env('THEME').'/img/articles/'.$obj->path);

            // Создаем изображение для просмотра конкретной статьи
            $img->fit(Config::get('settings.articles_img')['max']['width'],
                Config::get('settings.articles_img')['max']['height'])
                ->save(public_path().'/'.env('THEME').'/img/articles/'.$obj->max);

            // Создаем иконку для списка на основе соотношения сторон уже созданного "оригинала"
            $img_path = Image::make(public_path().'/'.env('THEME').'/img/articles/'.$obj->path);
            $img_path->fit(Config::get('settings.articles_img')['mini']['width'],
                Config::get('settings.articles_img')['mini']['height'])
                ->save(public_path().'/'.env('THEME').'/img/articles/'.$obj->mini);

            // Формируем json для хранения всех имен картинок в одной ячейке БД
            $json_imgs = json_encode($obj);

            return $json_imgs;
        }
    }


    // Физическое удаление файлов с диска
    protected function deleteFiles($images)
    {
        $dir_img = Storage::disk('images');

        foreach ($images as $image){

            if ($dir_img->has($image)){

                $dir_img->delete($image);
            }
        }
    }
}