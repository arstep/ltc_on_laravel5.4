<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Comment;
use App\Http\Controllers\MyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class CommentController extends MyController
{
    // Ajax запрос добавление комментария
    public function store(Request $request)
    {
        // Исключаем из массива в запросе токен и поля, которые называются неправильно
        // относительно модели Comment
        $data = $request->except('_token', 'comment_article_id', 'comment_parent_id');

        // Добавляем оставшиеся данные туда же, но с правильными ключами
        $data['article_id'] = $request->input('comment_article_id');
        $data['parent_id'] = $request->input('comment_parent_id');

        // Формируем набор правил валидации, общих для всех пользователей
        $validator = Validator::make($data, [

            'article_id' => 'integer|required',
//            'parent_id' => 'integer|required',
            'score' => 'integer|required',
//            'text' => 'required',
        ]);

        // Дополняем набор правил дополнительными полями (если сообщение
        // послано неаутентифицированным пользователем, то есть еще поля:
        $validator->sometimes('name', 'required|max:255', function ($input){
            // Если функция вернет true, то будут валидироваться указанные доп. поля
            return ! Auth::check();
        });
        $validator->sometimes('email', 'required|email', function ($input){
            // Если функция вернет true, то будут валидироваться указанные доп. поля
            return ! Auth::check();
        });

        // Проверяем наличие ошибок
        if ($validator->fails()){
            // Формируем ответ в виде JSON через абстракцию ответа Respond
            // $validator->errors() - возвращает объект ошибок, ->all() -
            // преобразует в массив
            return Response::json(['error' => $validator->errors()->all()]);
        }

        // Получаем объект аутентифицированного пользователя, если есть
        $user = Auth::user();

        // Создаем объект комментария по массиву аргументов
        $comment = new Comment($data);

        // Если отправитель аутентифицирован, то заполняем поле user_id
        if ($user){
            $comment->user_id = $user->id;
        }

        // При любом массовом сохранении не забыть описать свойства fillable в моделях
        $comment->save();

        // Посылаем ответ в виде JSON через абстракцию ответа Respond
        return Response::json(['success' => TRUE]);

        exit();
    }
}
