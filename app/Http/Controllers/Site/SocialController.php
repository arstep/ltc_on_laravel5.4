<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

class SocialController extends MyController
{
    // Методы для API VK
    public function login_vk()
    {
        return Socialite::with('vkontakte')->redirect();
    }
    public function callback_vk()
    {
        $user = Socialite::driver('vkontakte')->user();

        if ( ! is_array($user->user)){
            return redirect('/')->with('show_modal_msg','Ошибка авторизации');
        }

        $data = $user->user;
        $data['network'] = 'vkontakte';

        $this->authBySocial($data);

        return redirect('/cabinet/main');
    }





    // Методы для API OK
    public function login_ok()
    {
        return Socialite::with('odnoklassniki')->redirect();
    }
    public function callback_ok()
    {
        $user = Socialite::driver('odnoklassniki')->user();

        if ( ! is_array($user->user)){
            return redirect('/')->with('show_modal_msg','Ошибка авторизации');
        }

        $data['network'] = 'odnoklassniki';
        $data['uid'] = $user->user['uid'];
        $data['first_name'] = $user->user['first_name'];
        isset($user->user['email']) ? $data['email'] = $user->user['email'] : false;
        isset($user->user['pic190x190']) ? $data['photo'] = $user->user['pic190x190'] : false;

        $this->authBySocial($data);

        return redirect('/cabinet/main');
    }








    // Метод для ulogin.ru
    public function callback_ulogin()
    {

    	            $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
                    $user = json_decode($s, true);
                    //$user['network'] - соц. сеть, через которую авторизовался пользователь
                    //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
                    //$user['first_name'] - имя пользователя
                    //$user['last_name'] - фамилия пользователя

        $this->authBySocial($user);
    }









        // Общий метод для аутентификации
    private function authBySocial(array $user)
    {
        // Проверяем есть запись для такого пользователя
        if ($user_model = User::where(['network' => $user['network'],'uid' => $user['uid']])->first()) {

            Auth::login($user_model);

            if (isset($user['photo'])) {
            	$user_model->photo = $user['photo'];
            	$user_model->update();
            }            
        }
        elseif (isset($user['email']) && $user_model = User::where('email',$user['email'])->first()){

            Auth::login($user_model);

            $user_model->network = $user['network'];
            $user_model->uid = $user['uid'];
            isset($user['identity']) ? $user_model->identity = $user['identity'] : false;
            isset($user['first_name']) ? $user_model->name = $user['first_name'] : false;
            isset($user['photo']) ? $user_model->photo = $user['photo']: false;

            $user_model->update();
        }
        else {

            $user_model = new User();

            $user_model->network = $user['network'];
            $user_model->uid = $user['uid'];
            isset($user['identity']) ? $user_model->identity = $user['identity'] : false;
            isset($user['first_name']) ? $user_model->name = $user['first_name'] : false;
            isset($user['email']) ? $user_model->email = $user['email'] : false;
            isset($user['photo']) ? $user_model->photo = $user['photo']: false;

dd($user_model);

            $user_model->save();

            Auth::login($user_model);
        }

        return true;
    }
}
