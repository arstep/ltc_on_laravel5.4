<?php

namespace App\Http\Controllers\Site;

use App\Article;
use App\Comment;
use App\Http\Controllers\MyController;
use App\Http\Repositories\ArticlesImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends MyController
{
    use ArticlesImages;

    // Постраничная выборка статей с обратной сортировкой по id
    public function getArticles()
    {
        $paginates = Article::orderBy('id', 'desc')->paginate(5);

        $paginates = $this->addArrayImages($paginates);

        $data = [
            'titleHead' => 'Статьи о прогнозировании',
            'paginates' => $paginates,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME') . '.articles_list')) {
            return view(env('THEME') . '.articles_list', $data);
        }
    }




    // Одна статья и выборка комментариев к ней
    public function getArticle($id)
    {
        $article = Article::where('id', $id)->first();

        // Формируем данные для блока комментариев - ИСКЛЮЧИЛ !! т.к. не предусмотрено дизайном сайта
/*        $collection = Comment::where('article_id', $article->id)->orderBy('id', 'desc')->get();
        $comments = $collection->groupBy('parent_id');
        $comments_count = count($collection);*/

        $article = $this->addImage($article);

        $data = [
            'titleHead' => $article->title,
            'article' => $article,
/*            'comments' => $comments,
            'comments_count' => $comments_count,*/
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME') . '.article_single')) {
            return view(env('THEME') . '.article_single', $data);
        }
    }
}
