<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use App\Prediction;
use App\Typeofsport;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class PredictionsController extends MyController
{
    // Список прогнозов по всем видам спорта
    public function showAll()
    {
//        dd($this->subscribeList);

        // По каждому виду спорта формируем объект постраничной навигации прогнозов
        foreach ($this->listSport as $item){

            $arrPaginates[$item->name_en] = $this->getPagination($item);
        }

        $data = [
            'titleHead' => 'Прогнозы на спорт',
            'arrPaginates' => $arrPaginates,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.sport_predictions')) {
            return view(env('THEME').'.sport_predictions', $data);
        }
    }


    // Вывод одного прогноза по id
    public function singlePrediction($typeOfSport, $idPrediction)
    {
        // Модель прогноза
        $prediction = Prediction::find($idPrediction);

        // Получаем объект вида спорта
        $typeOfSport = Typeofsport::where('name_en', '=', $typeOfSport)->first();

        $data = [
            'titleHead' => 'Прогноз на ' . $typeOfSport->name_ru . ': ' . $prediction->team_one . '-' . $prediction->team_two,
            'prediction' => $prediction,
            'typeOfSport' => $typeOfSport,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.single_prediction')) {
            return view(env('THEME').'.single_prediction', $data);
        }
    }



    // Вывод прогнозов по виду спорта
    public function typeOfSport($typeOfSport)
    {
        // Получаем объект вида спорта
        $typeOfSport = Typeofsport::where('name_en', '=', $typeOfSport)->first();

        // Формируем объект постраничной навигации прогнозов
        $paginates =         $paginates = Prediction::where('type_of_sport', '=', $typeOfSport->name_en)
            ->orderBy('event_date', 'desc')
            ->paginate(1);

        // Загрузка следующих страниц Ajax-ом. Если запрос был передан асинхронно –
        // выдать рендер блока который нужно будет вставить
        if (Request::ajax()) {
            return Response::json(view(env('THEME') . '.pages_include.ajax_table_with_pagination', [
                'typeOfSport' => $typeOfSport,
                'paginates' => $paginates
            ])->render());
        }

        $data = [
            'titleHead' => 'Последние прогнозы на ' . $typeOfSport->name_ru,
            'typeOfSport' => $typeOfSport,
            'paginates' => $paginates,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.typeofsport_predictions')) {
            return view(env('THEME').'.typeofsport_predictions', $data);
        }
    }


    protected function getPagination($typeOfSport)
    {

        $paginates = Prediction::where('type_of_sport', '=', $typeOfSport->name_en)
            ->orderBy('event_date', 'desc')
            ->paginate(1);
        $paginates->withPath('predictions/'.$typeOfSport->name_en);

        return $paginates;
    }
}
