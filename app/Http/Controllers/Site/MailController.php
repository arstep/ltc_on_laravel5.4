<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    // Получение письма с формы сайта
    public function send(Request $request)
    {
        // Формируем правила валидации в зависимости от отправителя
        $rules = ['text' => 'required|string|max:5000',];

        // Если пользователь гость, то существуют еще поля
        if (Auth::guest()){

            $rules = array_merge($rules,[
                                            'name' => 'required|max:255',
                                            'email' => 'required|email'
                                        ]);
        }

        // Валидация. Если проблема - уйдет на REFERER с ошибками в сессии
        $this->validate($request,$rules);

        // Все ок - формируем массив для передачи в шаблон
        $data = $request->except('_token');

        if (Auth::check()){
            $user = Auth::user();
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['role'] = $user->role;
        }

        // $message всегда передается в шаблон, через нее можно прикреплять вложения
        Mail::send(env('THEME').'.mail', $data, function ($message){

            $message->to('arstep@mail.ru')->subject('Уведомление с сайта LTC!');
        });

        // Редирект с одноразовой сессией
        return redirect()->back()->with('success', 'Письмо отправлено!')->with('show_modal_msg', 'Письмо отправлено!');
    }
}
