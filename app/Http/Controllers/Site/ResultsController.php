<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResultsController extends MyController
{
    //
    public function index()
    {
        $data = [
            'titleHead' => 'Результаты прогнозов',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.results')) {
            return view(env('THEME').'.results', $data);
        }
    }


    public function singlePrediction()
    {

    }
}
