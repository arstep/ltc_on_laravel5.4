<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use App\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends MyController
{
    //
    public function index()
    {

        $subscribes = Subscribe::where('display','1')->get();

        // Верстка блоков подписок будет осуществлять попарно из-за адаптивности, поэтому
        // подготавливаем данные в виде вложенного массива, в каждой его ячейке по 2 записи
        for ($i=1; $i<=count($subscribes); $i++){

            $blockSubscribes[] = $subscribes[$i-1];

            if ($i%2 == 0){
                $arrSubscribes[] = $blockSubscribes;
                unset($blockSubscribes);
            }
        }
        isset($blockSubscribes) ? $arrSubscribes[] = $blockSubscribes : false;


        $data = [
            'titleHead' => 'Подписка на платные прогнозы',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
            'arrSubscribes' => $arrSubscribes,
        ];

        if (view()->exists(env('THEME').'.shop')) {
            return view(env('THEME').'.shop', $data);
        }
    }


    public function getSubscribe($id)
    {
        $subscribe = Subscribe::find($id);

        $data = [
            'titleHead' => 'Подписка на платные прогнозы',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
            'subscribe' => $subscribe,
        ];

        return view(env('THEME').'.shop_simple', $data);
    }
}
