<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use App\Subscribe;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CartController extends MyController
{
    /**
     * Показать страницу с корзиной
     */
    public function index()
    {
        //
        $data = [
            'titleHead' => 'Корзина',
            'goods' => Session::get('buy'),
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME') . '.cart_page')) {
            return view(env('THEME') . '.cart_page', $data);
        }
    }


    /**
     * Метод добавления/обновления товара в корзину AJAX запросом
     */
    public function addGoods(Request $request)
    {
        // Получаем количество дней для подписки
        $id = $request->id;

        // Получаем модель Подписка
        $subscribe = Subscribe::find($id);

        // Проверяем есть ли уже заказ в сессии, и совпадает ли id из запроса с id в заказе сессии
        if (Session::has('buy') && (Session::get('buy'))['id'] == $subscribe->id) {

            // Посылаем ответ в виде JSON через абстракцию ответа Respond
            $message = '<strong>' . $subscribe->title . '</strong> уже добавлена в корзину';
            return Response::json([
                'error' => TRUE,
                'message' => $message,
            ]);
        } else {
            // Иначе записываем/перезаписываем в сессию ключ bay
            Session::put('buy', [
                'id' => $subscribe->id,
                'value' => 1,
                'title' => $subscribe->title,
                'price' => $subscribe->price,
            ]);
            // Посылаем ответ в виде JSON через абстракцию ответа Respond
            return Response::json([
                'success' => TRUE,
                'title' => $subscribe->title,
                'price' => $subscribe->price,
            ]);
        }

        exit();
    }



    // Удаление товара из корзины
    public function deleteGoods(Request $request)
    {
        $id = $request->id;

        // Проверяем есть ли заказ в сессии, и совпадает ли id из запроса с id в заказе сессии
        if (Session::has('buy') && (Session::get('buy'))['id'] == $id){

            Session::forget('buy');
        }

        return redirect()->back();
    }



    // Оформление заказа
    public function completion()
    {
        if (Auth::guest()){
            return redirect('/login');
        }

        $user = User::find(Auth::id());

        $data = [
            'titleHead' => 'Корзина',
            'goods' => Session::get('buy'),
            'user' => $user,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME') . '.completion_page')) {
            return view(env('THEME') . '.completion_page', $data);
        }
    }

}
