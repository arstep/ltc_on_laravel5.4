<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CabinetController extends MyController
{
    //
    public function index()
    {
        $id = Auth::id();
        $user = User::with('comments')->find($id);


        $data = [
            'titleHead' => 'Личный кабинет',
            'user' => $user,
            'page' => 'main',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME') . '.cabinet')) {
            return view(env('THEME') . '.cabinet', $data);
        }
    }


    // Изменение пользовательских данных
    public function userData(Request $request)
    {
        $user = Auth::user();

        if ($request->has('name')){

            if ($user->name == $request->name){
                return redirect()->back();
            }

            $this->validate($request, [
                'name' => 'required|max:255',
            ]);

            $user->name = $request->name;
            $this->updateUser($user);
        }

        if ($request->has('email')){

            if ($user->email == $request->email){
                return redirect()->back();
            }

            $this->validate($request, [
                'email' => 'required|email|max:255|unique:users',
            ]);

            $user->email = $request->email;
            $this->updateUser($user);
        }

        if ($request->has('password')){

            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
            ]);

            $user->password = bcrypt($request->password);
            $this->updateUser($user);
        }

        return redirect()->back();
    }



    // AJAX. Отдает форму для модального окна на запрос изменения поля данных пользователя
    public function getModal()
    {
        if (request()->has('name')){
            return view(env('THEME') . '.pages_include.cabinet_ajax_modal')->with('name',request('name'));
        }
    }


    private function updateUser($user)
    {

        if ($user->update()){

            return redirect()->back()->with('show_modal_msg','Данные обновлены');
        }
        else{

            Log::error('Ошибка при обновлении данных пользователя '.$user->name.' в кабинете');
            return redirect()->back()->with('show_modal_msg','Ошибка записи');
        }
    }










    // Информация о пользователе и его ролях в удобном виде
/*    public static function getAuthUser()
    {
        $userModel = Auth::user();

        $roles = $userModel->roles;

        $rolesArr = [];

        foreach ($roles as $role){

            $rolesArr[$role->role] = $role->title;
        }

        return $user = ['model' => $userModel, 'roles' => $rolesArr];
    }*/
}
