<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\MyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;


class IndexController extends MyController
{
    //
    public function index()
    {
        $data = [
            'titleHead' => 'Прогнозы на спорт',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.index')) {
            return view(env('THEME').'.index', $data);
        }
    }


    public function about()
    {
        $data = [
            'titleHead' => 'Описание алгоритма',
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.about')) {
            return view(env('THEME').'.about', $data);
        }
    }


    public function reviews()
    {
        // Формируем данные для блока комментариев отдельно верхний уровень -
        // с обратной сортировкой и ответы к ним - без сортировки.
        // По возможности к ним присоединяются данные из таблицы аутент. пользователей
        // (если комментарий оставлял такой пользователь)
        $parent = Comment::where('parent_id', 0)
                            ->where('show_comment',1)
                            ->orderBy('id', 'desc')
                            ->with('user')
                            ->get();
        $reply = Comment::where('parent_id','>',0)
                            ->where('show_comment',1)
                            ->with('user')
                            ->get()
                            ->groupBy('parent_id');

        $comments_count = count(Comment::where('show_comment',1)->get());

        $data = [
            'titleHead' => 'Отзывы о прогнозах',
            'parent' => $parent,
            'reply' => $reply,
            'comments_count' => $comments_count,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        if (view()->exists(env('THEME').'.reviews')) {
            return view(env('THEME').'.reviews', $data);
        }
    }
}
