<?php

/*
 * Общие переменные которые нужны почти на всех страницах - для всех сайдбаров,
 * хэдеров, футеров и пр.
 * В версиях Ларавел старше 5.2 невозможно получить данные из Auth и Session в конструкторе
 * любого класса, поэтому информацию о пользователе и сессиях приходится добывать позже и как
 * придется.
 * */

namespace App\Http\Controllers;

use App\Prediction;
use App\Subscribe;
use App\Typeofsport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

abstract class MyController extends Controller
{
    // Перечень видов спорта - для всех меню, сайдбаров и футеров (коллекция моделей)
    protected $listSport = [];

    // Последние прогнозы - для сайдбара (коллекция моделей прогнозов)
    protected $lastPredicts = [];
    // Последние результаты - для сайдбара (коллекция моделей прогнозов)
    protected $lastResults = [];

    // Перечень платных подписок [][id, day, title, price]
    protected $subscribeList = [];



    public function __construct()
    {
        $this->listSport = $this->getListSport();

        // Два последних прогноза
        $this->lastPredicts = $this->getLastPredicts(2);

        // Два последних результата
        $this->lastResults = $this->getLastResult(2);

        $this->subscribeList = $this->getSubscribeList();

//        Log::info('$lastPredicts: ', ['$lastPredicts' => $this->lastPredicts]);
    }


    private function getLastPredicts($take)
    {
        return Prediction::whereNull('result')->where('commercial','0')->orderBy('event_date', 'desc')->take($take)->get();
    }


    private function getLastResult($take)
    {
        return Prediction::where('result','!=','0')->orderBy('event_date', 'desc')->take($take)->get();
    }


    private function getListSport()
    {
        return Typeofsport::all();
    }


    protected function getSubscribeList()
    {
        return Subscribe::where('display', '1')->orderBy('day', 'asc')->get();
    }
}
