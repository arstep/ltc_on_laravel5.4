<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class UserController extends AdminController
{

    public function __construct()
    {
        parent::__construct();

        $this->template = env('THEME') . '.admin.users';
    }


    public function index()
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_ADMIN')) {
            abort(403);
        }

        $this->title = 'Определение прав зарегистрированных пользователей';

        $users['none'] = User::where('role','none')->orderBy('id','desc')->get();
        $users['paid'] = User::where('role','paid')->orderBy('id','desc')->get();
        $users['moderator'] = User::where('role','moderator')->orderBy('id','desc')->get();
        $users['admin'] = User::where('role','admin')->orderBy('id','desc')->get();

        $this->content = view(env('THEME') . '.admin.users_content')
            ->with('title', $this->title)
            ->with('users', $users)
            ->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        echo __METHOD__;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo __METHOD__;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo __METHOD__;
    }







    /**
     * Show the form for editing the specified resource.
     * Просмотр деталей пользователя с возможностью изменения его роли
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_ADMIN')) {
            abort(403);
        }

        $user = User::with('comments')->find($id);

        $roles = [
            'none',
            'paid',
            'moderator',
            'admin',
        ];

        $this->title = 'Информация о пользователе';

        $this->content = view(env('THEME') . '.admin.users_single_content')
            ->with('title', $this->title)
            ->with('user', $user)
            ->with('roles', $roles)
            ->render();

        return $this->renderOutput();
    }







    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_ADMIN')) {
            abort(403);
        }

        $user = User::find($id);

        $user->role = $request->role;

        if ($user->update()){

            return redirect()->back()->with('status', 'Информация обновлена');
        }
        else{
            return redirect()->back()->with('error','Ошибка при сохранении в БД');
        }

        return $this->renderOutput();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_ADMIN')) {
            abort(403);
        }

        $user = User::find($id);

        if ($user->delete()){

            // Удалим его комментарии, но оставим его статьи.
            $user->comments()->delete();

            return redirect('/admin/users')->with('status', 'Пользователь '.$user->name.' был удален');
        }
        else {
            return redirect()->back()->with('error', 'Ошибка при удалении');

        }
    }
}
