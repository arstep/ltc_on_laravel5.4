<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class IndexController extends AdminController
{
    //

    public function __construct()
    {
        parent::__construct();

        $this->template = env('THEME').'.admin.index';
    }


    public function index()
    {
       // Устанавливаем и проверяем права текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $this->title = 'Панель администратора';

        return $this->renderOutput();
    }
}
