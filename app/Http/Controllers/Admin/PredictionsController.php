<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PredictionRequest;
use App\Prediction;
use App\Typeofsport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class PredictionsController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->template = env('THEME') . '.admin.users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $this->title = 'Прогнозы и результаты прогнозов';

        $predictions = Prediction::orderBy('event_date','desc')->take(50)->paginate(50);

        $this->content = view(env('THEME') . '.admin.predictions_index_content')
            ->with('title', $this->title)
            ->with('predictions', $predictions)
            ->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $this->title = 'Добавление прогноза';

        $type_of_sport = Typeofsport::all();

        $this->content = view(env('THEME') . '.admin.predictions_single_content')
            ->with('title', $this->title)
            ->with('type_of_sport', $type_of_sport)
            ->render();

        return $this->renderOutput();
    }






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PredictionRequest $request)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $data = $request->except('_token');

        $prediction = new Prediction($data);

        if ($prediction->save()){
            return redirect('/admin/predictions')->with('status', 'Прогноз был добавлен');
        }
        else{
            return redirect()->back()->with('error','Ошибка при сохранении в БД');
        }
    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo __METHOD__;
    }







    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $this->title = 'Редактирование прогноза';

        $prediction = Prediction::find($id);

        $type_of_sport = Typeofsport::all();

        $this->content = view(env('THEME') . '.admin.predictions_single_content')
            ->with('title', $this->title)
            ->with('prediction', $prediction)
            ->with('type_of_sport', $type_of_sport)
            ->render();

        return $this->renderOutput();
    }










    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PredictionRequest $request, $id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $data = $request->except('_token');

        $prediction = Prediction::find($id);

        $prediction->fill($data);

        if ($prediction->update()){
            return redirect('/admin/predictions')->with('status', 'Прогноз обновлен');
        }
        else{
            return redirect()->back()->with('error','Ошибка при сохранении в БД');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function destroy($id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Требуются права администратора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')) {
            abort(403);
        }

        $prediction = Prediction::find($id);

        if ($prediction->delete()){
            return redirect('/admin/predictions')->with('status', 'Прогноз удален');
        }
        else{
            return redirect()->back()->with('error','Ошибка при удалении');
        }
    }
}
