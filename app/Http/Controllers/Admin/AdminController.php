<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    //
    protected $user;

    protected $template;

    protected $content = false;

    protected $title;

    protected $data;


    public function __construct()
    {
        // Авторизация не работает в конструкторе, придется писать в каждом методе
        // $this->user = Auth::user();
    }

    // Проверяем и устанавливаем текущего пользователя
    protected function getUser()
    {
        $user = Auth::user();

        if ( ! $user) {
            abort(403);
        }

        return $user;
    }


    // Формируем содержимое страницы по частям, заполняя массив data
    public function renderOutput()
    {
        $this->data['title'] = $this->title;

        // Формируем названия и маршруты для пунктов меню
        $menu = $this->getMenu();

        // html код панели навигации
        $navigation = view(env('THEME') . '.admin.navigation')->with('menu', $menu)->render();
        $this->data['navigation'] = $navigation;

        // html код контента
        $this->data['content'] = $this->content;

        // html код футера
        $footer = view(env('THEME') . '.layouts.footer_admin')->render();
        $this->data['footer'] = $footer;

        return view($this->template)->with($this->data);
    }


    public function getMenu()
    {
        if ($this->user->role == 'admin') {

            return $menu = [
                'Dashboard' => '/admin',
                'Статьи' => '/admin/articles',
                'Прогнозы' => '/admin/predictions',
                'Комментарии' => '#',
                'Пользователи' => '/admin/users',
                'Заказы' => '#',
                'Почта' => '#',
            ];
        }
        elseif ($this->user->role == 'moderator') {

            return $menu = [
                'Dashboard' => '/admin',
                'Статьи' => '/admin/articles',
                'Прогнозы' => '/admin/predictions',
                'Комментарии' => '#',

                'Заказы' => '#',
                'Почта' => '#',
            ];
        }
    }
}
