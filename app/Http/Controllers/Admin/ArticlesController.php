<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Repositories\ArticlesImages;
use App\Http\Requests\ArticleRequest;
use App\Http\Repositories\Services;
use Illuminate\Support\Facades\Gate;

class ArticlesController extends AdminController
{
    use ArticlesImages;


    public function __construct()
    {
        parent::__construct();

        $this->template = env('THEME').'.admin.articles';
    }


    /**
     * Отображение списка ресурсов.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();

        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $this->title = 'Менеджер статей';

        $paginates = Article::orderBy('id','desc')->with('user')->paginate(5);

        // Дополняем модели фотографиями
        $paginates = $this->addArrayImages($paginates);

        $this->content = view(env('THEME') . '.admin.articles_content')
            ->with('paginates',$paginates)
            ->render();

        return $this->renderOutput();
    }

    /**
     * Показать форму для создания нового ресурса.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $this->title = 'Добавление новой статьи';

        $this->content = view(env('THEME') . '.admin.articles_create_content')->with('title',$this->title)->render();

        return $this->renderOutput();
    }

    /**
     * Запись нового материала в БД
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        // Валидация входных данных происходит в классе ArticleRequest

        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $data = $request->except('_token', 'image');

        // Если не введено поле alias, то мы его генерируем
        if (empty($data['alias'])){
            $data['alias'] = Services::transliterate($data['title']);
        }

        // Если в БД есть уже запись c таким alias (совпадение сгенерированного), возвращаемся на предыдущую страницу
        // показываем сгенерированный alias и просим ввести другое название или alias
        if (Article::where('alias', $data['alias'])->first()){

            // В объект Request добавляем массив содержащий сгенерированный alias
            $request->merge(['alias' => $data['alias']]);
            // Сохраним все данные в сессию
            $request->flash();

            return back()->with('error', 'Данный псевдоним уже существует в базе данных, измените его');
        };

        // Если был отправлен файл изображения добавляем его к общему массиву данных в виде строки Json
        if ($request->hasFile('image')){

            $data['img'] = $this->makeJsonImage($request);
        }

        // Сохраняем модель статьи через пользователя, к которому она привязывается отношением
        $article = new Article($data);
        if ($this->user->articles()->save($article)){
            return redirect('/admin/articles')->with('status', 'Материал добавлен');
        }
        else{
            return redirect()->back()->with('error','Ошибка при сохранении в БД');
        }
    }



    /**
     * Отображение ресурса.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo __METHOD__;
    }




    /**
     * Показать форму для редактирования указанного ресурса.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Устанавливаем и проверяем права текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $article = Article::with('user')->find($id);

        // Дополняем модель фотографиями
        $article = $this->addImage($article);

        $this->title = 'Редактирование статьи';

        $this->content = view(env('THEME') . '.admin.articles_create_content')
            ->with('article',$article)
            ->with('title',$this->title)
            ->render();

        return $this->renderOutput();
    }


    /**
     * Обновление указанного ресурса в БД.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        $article = Article::with('user')->find($id);

        $data = $request->except('_token', 'image', '_method');

        // Если был отправлен файл изображения добавляем его к общему массиву данных в виде строки Json
        if ($request->hasFile('image')){

            $data['img'] = $this->makeJsonImage($request);
            // Получим существующие картинки из БД в виде массива для удаления
            $old_images = json_decode($article->img, true);
        }

        $article->fill($data);

        if ($article->update()){
            if ( ! empty($old_images)) {
                $this->deleteFiles($old_images);
            }
            return redirect('/admin/articles')->with('status', 'Материал обновлен');
        }
        else{
            return redirect()->back()->with('error','Ошибка при сохранении в БД');
        }
    }

    /**
     * Удалить указанный ресурс.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        // Устанавливаем текущего пользователя
        $this->user = $this->getUser();
        // Достаточно прав модератора (проверка в AuthServiceProvider)
        if (Gate::denies('ALL_MODERATOR')){
            abort(403);
        }

        // Если будут подключены комментарии к статьям, предусмотреть код их удаления
//        $article->comments()->delete();

        // Получим существующие картинки из БД в виде массива для удаления
        $article_images = json_decode($article->img, true);

        if ($article->delete()){

            if ( ! empty($article_images)){
                $this->deleteFiles($article_images);
            }

            return redirect('/admin/articles')->with('status', 'Материал удален');
        }
        else{
            return redirect()->back()->with('error','Ошибка при удалении');
        }
    }
}
