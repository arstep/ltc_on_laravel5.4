<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MyController;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Request;

class ResetPasswordController extends MyController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    // Перегружаем метод из трейта и добавляем ему свои переменные для меню,
    // сайдбаров и футера. Перед этим изменив родительский класс.
    public function showResetForm(Request $request, $token = null)
    {
        $data = [
            'token' => $token,
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        return view('auth.passwords.reset', $data);
    }
}
