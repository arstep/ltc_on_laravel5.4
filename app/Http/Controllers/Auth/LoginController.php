<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MyController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends MyController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }



    // Перегружаем метод из трейта и добавляем ему свои переменные для меню,
    // сайдбаров и футера. Перед этим изменив родительский класс.
    public function showLoginForm()
    {
        $data = [
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        return view('auth.login', $data);
    }


    // Перегружаем метод из трейта отрабатывающий сразу после аутентификации пользователя
    // *пока не нашел способа перенаправления на страницу с которой инициирована
    // аутентификация без переписывания ядра, которое может обновиться
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        // Если пользователь идентифицировался имея заказ, то направляем его в корзину
        if (Session::has('buy')){
            return redirect('/cart');
        }
    }

}
