<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MyController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends MyController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    // Перегружаем метод из трейта и добавляем ему свои переменные для меню,
    // сайдбаров и футера. Перед этим изменив родительский класс.
    public function showLinkRequestForm()
    {
        $data = [
            'listSport' => $this->listSport,
            'lastPredicts' => $this->lastPredicts,
            'lastResults' => $this->lastResults,
            'subscribeList' => $this->subscribeList,
        ];

        return view('auth.passwords.email', $data);
    }
}
