<?php

namespace App\Http\Requests;

use App\Article;
use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Нет дополнительной проверки права - ставим true
        return true;
    }


    // Переопределяем родительский метод для возможности добавления
    // условия валидации в методе rules()
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        // Добавляем правила проверки поля alias на уникальность
        $validator->sometimes('alias', 'unique:articles|max:255', function ($input){

            // Если в маршруте есть параметр article(это id), значит имеем дело с
            // обновлением материала. Сравниваем поле alias в БД и в запросе, если
            // не совпадает, значит изменилось - добавляем проверку на уникальность
            if ($this->route()->hasParameter('article')){
                $model = Article::find($this->route()->parameter('article'));

                return ($model->alias !== $input->alias && !empty($input->alias));
            }

            // В противном случае проверяем это поле только если оно заполнено в запросе
            return !empty($input->alias);
        });

        return $validator;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Дополнительное условие валидации добавили в переопределенный метод getValidatorInstance()

        return [
            //
            'title' => 'required|max:255',
            'text' => 'required',
        ];
    }
}
