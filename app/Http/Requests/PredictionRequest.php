<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PredictionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'commercial' => 'required',
            'event_date' => 'required|date',
            'type_of_sport' => 'required|string',
            'region' => 'required|string',
            'team_one' => 'required|string',
            'team_two' => 'required|string',
            'rate' => 'required|numeric',
        ];
    }
}
