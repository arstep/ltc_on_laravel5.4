<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
/*        DB::listen(function ($query){
            // $query - объект SQL запроса
            dump($query->sql); // - распечатывает все строки запросов которые будут выполняться
            dump($query->bindings); // - массив параметров которые передаются в запрос
        });*/

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
