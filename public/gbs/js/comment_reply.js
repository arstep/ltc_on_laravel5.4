/*
 * Объект для работы с кнопкой ответить на комментарий
 * */
var getForm = {
    // Метод для притяжения формы ввода ответа под комментарий
    move: function (event, id) {

        // отмена стандартного события
        event.preventDefault();

        $('#form_comment_title').hide();

        // Устанавливаем id родительского комментария в скрытое поле формы
        $('#comment_parent_id').val(id);

        // Вставляем форму рядом с кнопкой на которую нажали
        var form = $('#respond');
        $(event.target).after(form);

        // Новая кнопка возврат формы вниз
        var backButton = '<a href="#" id="backButton" onclick="return getForm.back()"><i class="glyphicon glyphicon-remove" aria-hidden="true" style="color: tomato; font-size: 1.3em"></i></a>';

        // Добавляем в форму кнопку отменить ответ, с проверкой на уже существование такой кнопки
        if (!$('a').is('#backButton'))
            $('#respond').prepend(backButton);
    },

    // Метод для возврата формы вниз страницы, если передумали отвечать на комментарий
    back: function () {

        $('#form_comment_title').show();

        // Сбрасываем id родительского комментария в скрытом поле формы на 0
        $('#comment_parent_id').val('0');

        var form = $('#respond');

        // Двигаем форму обратно вниз
        $('#formcomment').append(form);

        // Удаляем кнопку отмены ответа на комментарий
        $('#backButton').remove();

        // Отменяем отправку формы на сервер
        return false;
    }
}

