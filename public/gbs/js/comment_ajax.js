$(function () {

    // Отправка комментария AJAX запросом
    $('#form-send-comment').on('click', '#submit', function (event) {

        event.preventDefault();

        // В блоке modal показываем этапы отправки формы
        $('.modal-title').html('Сохранение комментария');
        // Показ модального окна
        $('#modal').modal();

            // Непосредственно сама отправка формы
            // Возьмем содержимое формы в виде массива объектов
            var data = $('#form-send-comment').serializeArray();

            $.ajax({

                // маршрут берем из аттрибута, т.к. он формируется шаблонизатором Blade и может измениться
                url: $('#form-send-comment').attr('action'),
                data: data,
                type: 'POST',
                datatype: 'JSON',
                // При ответе сервера 200 ОК мы предусмотрели два варианта своих действий
                success: function (html) {
                    // Если посланы ошибки
                    if (html.error){

                        // Ошибки приходят в виде массива, склеиваем в строку через <br>
                        $('.modal-body p')
                            .css('color','red')
                            .html('<strong>Есть ошибки!</strong><br>' + html.error.join('<br>'));
                    }
                    // Если все нормально
                    else if(html.success){

                        // Сообщаем о сохранении
                        $('.modal-body p')
                            .css('color','green')
                            .html('<strong>Спасибо! Ваш комментарий будет добавлен после модерации.</strong>');

                                // Имитируем нажатие кнопки вернуть форму ответа вниз
                                $('#backButton').click();
                                // Очищаем форму
                                $('#form-send-comment')[0].reset()
                    }
                },
                error: function () {
                    // Если запрос неудачен
                    $('.modal-body p')
                        .css('color','red')
                        .html('<strong>Ошибка сохранения на сервере</strong>');
                        // Имитируем нажатие кнопки фернуть форму ответа вниз
                        $('#backButton').click();
                }
            })
    })
});


