$(function () {

    $('.table_with_pagination').on('click', '.pagination a', function (event) {

        event.preventDefault();

        // Поиск типа спорта в аттрибутах прародителя
        typeSport = $(this).parent().parent().parent().attr('data-sport');

        attrInsert = '[data-sport='+typeSport+']';

        url_path = $(this).attr('href');

        $.ajax({

            url: url_path,
            datatype: 'JSON',

            success: function (data) {
                $(attrInsert).html(data);
            },

            error: function () {
                $(attrInsert).html('<h4>Ошибка загрузки</h4>');
            }
        })
    })
})