var Cart = {

    // Добавление/перезапись заказа
    add: function (event, id) {

        event.preventDefault();

        $.ajax({

            url: '/cart/add',
            data: {'id': id},
            type: 'POST',
            datatype: 'JSON',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

            success: function (response) {

                // На сервере записали товар в сессию
                if (response.success) {

                    // Сообщение в модальном окне
                    $('.modal-body p')
                        .css('color', 'green')
                        .html('<strong>' + response.title + '</strong>' + ' добавлена в корзину!');

                    // Если еще нет кнопки в модальном окне - добавляем
                    if ( ! $('.modal .btn').is('.goto'))
                        $('.modal-footer').prepend('<a href="/cart" class="btn btn-primary goto">Перейти</a>');

                    // Список в корзине сайдбара
                    $('#cart_list').html('<p><i class="glyphicon glyphicon-ok" aria-hidden="true"></i>&nbsp;&nbsp;'
                        + response.title +'</p><h4>' + response.price + '  &#8381;</h4>');

                    // Если кнопки в сайдбаре еще нет - добавляем
                    if ( ! $('.cart .btn').is('.goto'))
                        $('.cart').append('<a href="/cart" class="btn btn-primary goto">Перейти</a>')
                }
                // В сессии уже был этот товар, сообщение об этом с сервера
                else if (response.error) {

                    $('.modal-body p')
                        .css('color', '#999')
                        .html(response.message);

                    // Если еще нет кнопки в модальном окне - добавляем
                    if ( ! $('.modal .btn').is('.goto'))
                        $('.modal-footer').prepend('<a href="/cart" class="btn btn-primary goto">Перейти</a>')
                }
            },

            error: function () {

                // Если запрос неудачен
                $('.modal-body p')
                    .css('color', 'red')
                    .html('<strong>Ошибка сервера</strong>');
            }
        });

        // Показ модального окна
        $('#modal').modal();

    },


    // Удаление позиции из корзины
    delete: function (obj) {

        name = $(obj).parent().attr('data-good');

        $('.modal-body').html('<h5>Вы действительно хотите убрать из корзины:</h5><strong>' +name+ ' ?</strong>');

        $('#modal').modal();

        return false;
    },
}
