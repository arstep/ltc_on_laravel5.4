function show_modal_msg(msg) {

    var divModal =
        "<div id='modal-msg' class='modal fade'>" +
            "<div class='modal-dialog' style='margin-top: 80px'>" +
                "<div class='modal-content'>" +
                    "<div class='modal-body'>" +
                    "</div>" +
                    "<div class='modal-footer'>" +
                        "<button type='button' class='btn btn-primary' data-dismiss='modal'>Закрыть</button>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</div>";

    $('body').append(divModal);

    $('#modal-msg .modal-body').html(msg);

    $('#modal-msg').modal();
}