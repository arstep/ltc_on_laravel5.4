<?php

// Свои произвольные параметры
return [

    // Размеры для картинок статей на сайте
    'articles_img' => [
        'max' => ['width'=>845, 'height'=>130],
        'mini' => ['width'=>150, 'height'=>150],
    ],

    // Размеры для картинок статей "оригинал на сервере"
    'image' => [
        'width' => 1024,
        'heigth' => 768,
    ],

    // Картинка по умолчанию для статей
    'articles_default_img' => [
        'max' => 'articles_default_img_max.jpg',
        'mini' => 'articles_default_img_mini.jpg'
    ],

];