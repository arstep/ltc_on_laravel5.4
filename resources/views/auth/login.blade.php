{{--@extends('layouts.app')--}}
@extends(env('THEME').'.layouts.rightsidebar')

@section('content')
{{--<div class="container">--}}
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Вход</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Войти
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Забыли пароль?
                                </a>
                            </div>
                        </div>
                    </form>


                    <a href="/social/login_vk"><img src="{{ asset(env('THEME')) }}/img/vk.png" width="60"></a>
                    <a href="/social/login_ok"><img src="{{ asset(env('THEME')) }}/img/ok.png" width="60"></a>


{{--
                    <script src="//ulogin.ru/js/ulogin.js"></script>
                    <div id="uLogin"
                         data-ulogin="display=panel;theme=flat;fields=first_name,last_name,sex,city,email;optional=photo;providers=vkontakte,odnoklassniki,facebook,twitter;redirect_uri=http%3A%2F%2Farstepfz.beget.tech%2Fsocial%2Fcallback_ulogin;mobilebuttons=0;">
                    </div>
--}}                    
                </div>
            </div>
        </div>
    </div>
{{--</div>--}}
@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.sendmail')
    @include(env('THEME').'.sidebar.search')
@endsection