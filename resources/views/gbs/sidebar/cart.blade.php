<div class="well sidebar-nav cart">
    <h5><i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;КОРЗИНА</h5>

    {{--Проверяем корзину перед отображением--}}
    @if(\Illuminate\Support\Facades\Session::has('buy'))
        <div id="cart_list">
            <p>
                <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>
                &nbsp;
            {{ (\Illuminate\Support\Facades\Session::get('buy'))['title'] }}

            <h4>
                {{ (\Illuminate\Support\Facades\Session::get('buy'))['price'] }}
                &#8381;
            </h4>
            </p>
        </div>

        <a href="/cart" class="btn btn-primary goto">Перейти</a>
    @else
        <div id="cart_list"><p>Корзина пуста</p></div>
    @endif

</div><!--/.well -->