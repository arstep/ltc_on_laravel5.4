<div class="well sidebar-nav">
    <h5>ПОДПИСАТЬСЯ НА ПЛАТНЫЕ ПРОГНОЗЫ LTC</h5>

{{--    {{ dump($subscribeList) }}--}}

    @foreach($subscribeList as $item)
        <div class="media">
            <a class="pull-left" href="/shop/{{ $item->id }}">
                <img class="media-object" src="{{ asset(env('THEME')) }}/img/LTC100x100.png" height="60">
            </a>
            <div class="media-body">
                <a href="/shop/subscribe/{{ $item->id }}" class="media-heading">{{ $item->title }}</a>
                <p><strong>{{ $item->price }} &#8381;</strong></p>
            </div>
        </div>
    @endforeach

</div><!--/.well -->