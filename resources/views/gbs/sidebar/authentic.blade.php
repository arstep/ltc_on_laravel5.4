<div class="well sidebar-nav">
    <h5>Авторизоваться</h5>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-3">Email</label>
            <div class="col-xs-9">
                <input type="password" class="form-control" id="inputEmail" placeholder="Логин">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-xs-3">Пароль</label>
            <div class="col-xs-9">
                <input type="password" class="form-control" id="inputPassword" placeholder="Пароль">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-1 col-xs-11">
                <div class="checkbox">
                    <label><input type="checkbox"> Запомнить</label>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>
</div><!--/.well -->