<div class="well sidebar-nav">
    <h5>ОБРАТНАЯ СВЯЗЬ</h5>

    @if (\Illuminate\Support\Facades\Session::has('success'))
        <strong class="text-success">
            {{ \Illuminate\Support\Facades\Session::get('success') }}
        </strong>

    @endif

    <form id="mail_send" action="/mail" method="post">

        @if( \Illuminate\Support\Facades\Auth::guest())
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Ваше имя</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Имя"
                       value="{{ old('name') }}" required>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Ваш Email</label>
                <input type="text" name="email" class="form-control" id="email" placeholder="Email"
                       value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        @endif


        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
            <label for="text">Сообщение</label>
            <textarea id="text" name="text" class="form-control" rows="5" required>{{ old('text') }}</textarea>

            @if ($errors->has('text'))
                <span class="help-block">
                        <strong>{{ $errors->first('text') }}</strong>
                    </span>
            @endif
        </div>

        {{ csrf_field() }}

        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>

</div><!--/.well -->