<div class="well sidebar-nav">
    <h5>ВИДЫ СПОРТА</h5>
    <ul class="nav nav-list">

        @foreach($listSport as $item)
            <li><a href="/predictions/{{ $item->name_en }}">Прогнозы на {{ $item->name_ru }}</a></li>
        @endforeach

    </ul>
</div><!--/.well -->