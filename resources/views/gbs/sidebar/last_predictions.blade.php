<div class="well sidebar-nav hidden-xs">
    <h5>СВЕЖИЕ ПРОГНОЗЫ И РЕЗУЛЬТАТЫ</h5>

    <ul class="nav nav-list asb">

        @foreach($lastPredicts as $item)
            <li>
                <a href="{{ route('prediction', ['typeOfSport' => $item->type_of_sport, 'idPrediction' => $item->id]) }}">
                    Бесплатный спортпрогноз на {{ date('d.m.Y', strtotime($item->event_date)) }} <br>
                    <p>{{ $item->team_one }} - {{ $item->team_two }}</p></a>
            </li>
        @endforeach


        @foreach($lastResults as $item)
            <li>
{{--                <a href="{{ route('result', ['type' => $item->commercial ? 'paid' : 'free', 'id' => $item->id]) }}">--}}
                <a href="">
                    Результат
                    {{ $item->commercial ? ' платного ' : ' бесплатного ' }}
                    спортпрогноза на {{ date('d.m.Y', strtotime($item->event_date)) }} <br>
                    <p>{{ $item->team_one }} - {{ $item->team_two }}</p></a>
            </li>
        @endforeach


    </ul>
</div><!--/.well -->