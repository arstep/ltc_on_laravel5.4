@extends('gbs.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active">
            <span>
                Личный кабинет
            </span>
        </li>
    </ul>


    {{--Секция вывода ошибок--}}
    @if(count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif



    <h3>Личный кабинет</h3>

    <br>

    <div class="row panel panel-default">
        <div class="col-md-4">

            <h4 class="page-header">Общие данные</h4>
            
            <br>

<div class="table table-responsive">
                <table class="table cabinet user">
                    <tr>
                        <td></td>
                        <td>
                            @if(isset($user->photo))
                                <img class="img-responsive" width="50"
                                     src="{{ $user->photo }}">
                            @else
                                <img class="img-responsive" width="50"
                                     src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{ $user->name }}</td>
                        <td>
                            <a href="#" onclick="$('#modal_user_form').load('/cabinet/getModal','name=name');$('#modal_user_form').modal();return false">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>
                            @if(isset($user->email))
                               {{ $user->email }}
                            @endif
                        </td>
                        <td>
                            <a href="#" onclick="$('#modal_user_form').load('/cabinet/getModal','name=email');$('#modal_user_form').modal();return false">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Пароль</td>
                        <td>
                            @if(isset($user->password))
                                ******
                            @endif
                        </td>
                        <td>
                            <a href="#" onclick="$('#modal_user_form').load('/cabinet/getModal','name=password');$('#modal_user_form').modal();return false">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Создан</td>
                        <td>{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                    </tr>
                    <tr>
                        <td>Изменен</td>
                        <td>{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="col-md-4">
            <h4 class="page-header">Подписки</h4>
        </div>

        <div class="col-md-4">
            <h4 class="page-header">Сообщения пользователя</h4>

            <br>

            <dl>
                @foreach($user->comments as $comment)
                    {{--<dt>Идентификатор статьи: {{ $comment->article_id }}</dt>--}}
                    <dt>Оценка: {{ $comment->score }}</dt>
                    <dd>
                        <small>{{ date('d.m.Y', strtotime($user->created_at)) }}</small>
                    </dd>
                    <dd>{{ $comment->text }}</dd>
                @endforeach
            </dl>

        </div>
    </div>



    {{--Модальное окно с формой для изменения информации о пользователе - вставка Ajax--}}
    <div id="modal_user_form" class="modal fade">

    </div><!-- /.modal -->
    

    

@endsection


@section('sidebar')

    @if($user->role == 'moderator' || $user->role == 'admin')
        <a href="/admin" class="btn btn-danger btn-lg center-block">Администрирование</a>
    @else

        @include(env('THEME').'.sidebar.dispath')
        @include(env('THEME').'.sidebar.listsport')
        @include(env('THEME').'.sidebar.sendmail')
{{--        @include('gbs.sidebar.search')--}}
    @endif
@endsection
