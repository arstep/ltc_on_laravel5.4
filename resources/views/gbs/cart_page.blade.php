@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/shop">Купить</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Корзина</span></li>
    </ul>

    <h3>Корзина</h3>

    <br>

    @if($goods)
        <div id="cart_content">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th>Наименование</th>
                        <th>Стоимость</th>
                        <th><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></th>
                    </tr>
                    <tr>
                        <td><a href="/shop/subscribe/{{ $goods['id'] }}">{{ $goods['title'] }}</a></td>
                        <td>{{ $goods['price'] }} &#8381;</td>
                        <td data-good="{{$goods['title']}}">
                            <a href="/cart/delete/{{$goods['id']}}" onclick="return Cart.delete(this)"><i
                                        class="glyphicon glyphicon-remove" aria-hidden="true"
                                        style="color: tomato;"></i></a></td>
                    </tr>
                </table>
            </div>

            @if(\Illuminate\Support\Facades\Auth::guest())
                <a class="btn btn-primary" disabled="disabled">Оформить заказ</a>
                <a href="/login" class="btn btn-primary">Войти</a>
                <br>
                <br>
                <p>* Вы не авторизованы. Для оформления заказа Вам необходимо иметь учетную запись на сайте, т.к. доступ к
                    платным прогнозам будет предоставляться при входе на сайт как авторизованный пользователь</p>
            @else
                <a href="/cart/completion" class="btn btn-primary">Оформить заказ</a>
            @endif

        </div>
    @else
        <h4>Корзина пуста</h4>
    @endif

    <div id="modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <a href="/cart/delete/{{$goods['id']}}" class="btn btn-default goto">Убрать</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.sendmail')
    @include(env('THEME').'.sidebar.dispath')
{{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
