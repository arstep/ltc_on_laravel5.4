{{--{{ dd($data) }}--}}

<h3 class="text-center">{{ $title }}</h3>

{{--        <form action="{{(isset($article->id)) ? '/admin/articles/update/'.$article->id : '/admin/articles/create' }}" method="get">--}}
        <form role="form" action="{{(isset($article->id)) ? '/admin/articles/'.$article->id : '/admin/articles' }}" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <label for="title">Название:</label>
                <small class="help-block">Заголовок материала</small>
                <input id="title" type="text" name="title" class="form-control" value="{{ (isset($article->title)) ? $article->title : old('title') }}">
            </div>

            <div class="form-group">
                <label for="alias">Псевдоним:</label>
                <small class="help-block">Псевдоним для строки URL (если используется)</small>
                <input id="alias" type="text" name="alias" class="form-control" value="{{ (isset($article->alias)) ? $article->alias : old('alias') }}">
            </div>

            <div class="form-group">
                <label for="description">Описание:</label>
                <small class="help-block">Введите краткое описание</small>
                <textarea name="description" id="description" class="form-control" rows="3">{!! (isset($article->description)) ? $article->description : old('description') !!}</textarea>
            </div>

            <div class="form-group">
                <label for="text">Текст</label>
                <small class="help-block">Введите текст статьи</small>
                <textarea name="text" id="text" class="form-control" rows="10">{!! (isset($article->text)) ? $article->text : old('text') !!}</textarea>
            </div>

            <label for="image">Изображение:</label>
            @if(isset($article->img->mini))
                <br>
                <img src="{{ asset(env('THEME')) }}/img/articles/{{ $article->img->mini }}">
                <br>
                <small>{{ $article->img->mini }}</small>
                <br>
                <img src="{{ asset(env('THEME')) }}/img/articles/{{ $article->img->max }}">
                <br>
                <small>{{ $article->img->max }}</small>
                {{--<input type="hidden" name="old_image" value="{{ $article->img->mini }}">--}}
            @endif

            <div class="form-group">
                <small class="help-block">Выберите изображение к статье (формат будет преобразован в jpeg)</small>
                <input id="image" type="file" name="image" class="filestyle" data-buttonText="Ищем файл">
            </div>


            @if(isset($article->id))
                {{--<input type="hidden" name="_method" value="PUT">--}}
                {{ method_field('PUT') }}
            @endif

            {{csrf_field()}}

            <button type="submit" class="btn btn-success">Сохранить</button>

        </form>

        <br>

        <script>
            CKEDITOR.replace('description');
            CKEDITOR.replace('text');
        </script>
