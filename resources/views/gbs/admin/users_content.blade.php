<h3 class="text-center">{{ $title }}</h3>

<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li class="active">
        <a href="#admin" role="tab" data-toggle="tab"><os-p>Администраторы</os-p></a>
    </li>
    <li>
        <a href="#moderator" role="tab" data-toggle="tab"><os-p>Модераторы</os-p></a>
    </li>
    <li>
        <a href="#paid" role="tab" data-toggle="tab"><os-p>Платные пользователи</os-p></a>
    </li>
    <li>
        <a href="#none" role="tab" data-toggle="tab"><os-p>Бесплатные пользователи</os-p></a>
    </li>
</ul>


<div id="myTabContent" class="tab-content">

    <div class="tab-pane fade in active" id="admin">
        <br>
        <div class="table-responsive">
            <table class="table table-bordered admin users">
                <tr style="background-color: #eee">
                    <th>ID</th>
                    <th>Аватар</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Права</th>
                    <th>Создан</th>
                    <th>Изменен</th>
                </tr>
                @foreach($users['admin'] as $user)
                    <tr>
                        <td class="text-center">{{ $user->id }}</td>
                        <td class="text-center">
                            @if(isset($user->photo))
                                <img class="img-responsive" width="100"
                                     src="{{ $user->photo }}">
                            @else
                                <img class="img-responsive" width="100"
                                     src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                        </td>
                        <td class="text-center">{{ $user->email }}</td>
                        <td class="text-center">{{ $user->role }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="tab-pane fade" id="moderator">
        <br>
        <div class="table-responsive">
            <table class="table table-bordered admin users">
                <tr style="background-color: #eee">
                    <th>ID</th>
                    <th>Аватар</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Права</th>
                    <th>Создан</th>
                    <th>Изменен</th>
                </tr>
                @foreach($users['moderator'] as $user)
                    <tr>
                        <td class="text-center">{{ $user->id }}</td>
                        <td class="text-center">
                            @if(isset($user->photo))
                                <img class="img-responsive" width="100"
                                     src="{{ $user->photo }}">
                            @else
                                <img class="img-responsive" width="100"
                                     src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                        </td>
                        <td class="text-center">{{ $user->email }}</td>
                        <td class="text-center">{{ $user->role }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="tab-pane fade" id="paid">
        <br>
        <div class="table-responsive">
            <table class="table table-bordered admin users">
                <tr style="background-color: #eee">
                    <th>ID</th>
                    <th>Аватар</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Права</th>
                    <th>Создан</th>
                    <th>Изменен</th>
                </tr>
                @foreach($users['paid'] as $user)
                    <tr>
                        <td class="text-center">{{ $user->id }}</td>
                        <td class="text-center">
                            @if(isset($user->photo))
                                <img class="img-responsive" width="100"
                                     src="{{ $user->photo }}">
                            @else
                                <img class="img-responsive" width="100"
                                     src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                        </td>
                        <td class="text-center">{{ $user->email }}</td>
                        <td class="text-center">{{ $user->role }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="tab-pane fade" id="none">
        <br>
        <div class="table-responsive">
            <table class="table table-bordered admin users">
                <tr style="background-color: #eee">
                    <th>ID</th>
                    <th>Аватар</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Права</th>
                    <th>Создан</th>
                    <th>Изменен</th>
                </tr>
                @foreach($users['none'] as $user)
                    <tr>
                        <td class="text-center">{{ $user->id }}</td>
                        <td class="text-center">
                            @if(isset($user->photo))
                                <img class="img-responsive" width="100"
                                     src="{{ $user->photo }}">
                            @else
                                <img class="img-responsive" width="100"
                                     src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                        </td>
                        <td class="text-center">{{ $user->email }}</td>
                        <td class="text-center">{{ $user->role }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                        <td class="text-center">{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

</div>

