<h3 class="text-center">{{ $title }}</h3>

<div class="row">

    <div class="col-sm-8">

        <form role="form" class="form-horizontal"
              action="{{(isset($prediction->id)) ? '/admin/predictions/'.$prediction->id : '/admin/predictions' }}"
              method="post">

            <div class="panel panel-success">
                <div class="panel-heading">Ввод типа прогноза</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="commercial" class="col-sm-3 control-label">Тип</label>
                        <div class="col-sm-4">
                            <select name="commercial" id="commercial" class="form-control">
                                @if(isset($prediction->commercial))
                                    <option value="0"{{ ($prediction->commercial == 0) ? ' selected' : false }}>
                                        Бесплатный
                                    </option>
                                    <option value="1" {{ ($prediction->commercial == 1) ? ' selected' : false }}>Платный
                                    </option>
                                @else
                                    <option value="">Укажите тип</option>
                                    <option value="0"{{ (old('commercial') == '0') ? ' selected' : false }}>Бесплатный
                                    </option>
                                    <option value="1"{{ (old('commercial') == '1') ? ' selected' : false }}>Платный
                                    </option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="well">
                <div class="form-group">
                    <label for="event_date" class="col-sm-3 control-label">Дата события</label>
                    <div class="col-sm-4">
                        <input id="event_date" type="date" class="form-control"
                               name="event_date"
                               value="{{(isset($prediction->event_date)) ? date('Y-m-d', strtotime($prediction->event_date)) : old('event_date') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="type_of_sport" class="col-sm-3 control-label">Вид спорта</label>
                    <div class="col-sm-4">
                        <select name="type_of_sport" id="type_of_sport" class="form-control">
                            @foreach($type_of_sport as $item)
                                @if(isset($prediction->id))
                                    <option value="{{ $item->name_en }}"{{ ($item->name_en == $prediction->type_of_sport) ? ' selected' : false }}>{{ $item->name_ru }}</option>
                                @else
                                    <option value="{{ $item->name_en }}"{{ ($item->name_en == old('type_of_sport')) ? ' selected' : false }}>{{ $item->name_ru }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <br>

                <div class="form-group">
                    <label for="region" class="col-sm-3 control-label">Место проведения</label>
                    <div class="col-sm-9">
                        <input id="region" type="text" class="form-control" name="region"
                               value="{{ (isset($prediction->region)) ? $prediction->region : old('region') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="league" class="col-sm-3 control-label">Лига</label>
                    <div class="col-sm-9">
                        <input id="league" type="text" class="form-control" name="league"
                               value="{{ (isset($prediction->league)) ? $prediction->league : old('league') }}">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="team_one" class="control-label">Команда 1</label>
                        <input id="team_one" type="text" class="form-control" name="team_one"
                               value="{{ (isset($prediction->team_one)) ? $prediction->team_one : old('team_one') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="team_two" class="control-label">Команда 2</label>
                        <input id="team_two" type="text" class="form-control" name="team_two"
                               value="{{ (isset($prediction->team_two)) ? $prediction->team_two : old('team_two') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Код</label>
                    <div class="col-sm-4">
                        <input id="code" type="text" class="form-control" name="code"
                               value="{{ (isset($prediction->code)) ? $prediction->code : old('code') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="rate" class="col-sm-3 control-label">Коэффициент</label>
                    <div class="col-sm-4">
                        <input id="rate" type="text" class="form-control" name="rate"
                               value="{{ (isset($prediction->rate)) ? $prediction->rate : old('rate') }}">
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Ввод результата выданного прогноза</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="result" class="col-sm-3 control-label">Результат</label>
                        <div class="col-sm-4">
                            <select name="result" id="result" class="form-control">
                                @if(isset($prediction->result))
                                    <option value="" selected></option>
                                    <option value="win"{{ ($prediction->result == 'win') ? ' selected' : false }}>
                                        Выигрыш
                                    </option>
                                    <option value="refund" {{ ($prediction->result == 'refund') ? ' selected' : false }}>
                                        Возврат
                                    </option>
                                    <option value="loss" {{ ($prediction->result == 'loss') ? ' selected' : false }}>
                                        Проигрыш
                                    </option>
                                @else
                                    <option value="" selected></option>
                                    <option value="win">Выигрыш</option>
                                    <option value="refund">Возврат</option>
                                    <option value="loss">Проигрыш</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>


            @if(isset($prediction->id))
                {{ method_field('PUT') }}
            @endif

            {{csrf_field()}}

            <button type="submit" class="btn btn-success center-block">Сохранить</button>

        </form>
    </div>


    <div class="col-sm-4">
        <dl class="dl-horizontal">
            <dt>Создан:</dt>
            <dd>{{ isset($prediction->created_at) ? date('d.m.Y', strtotime($prediction->created_at)) : false }}</dd>
            <dt>Изменен:</dt>
            <dd>{{ isset($prediction->updated_at) ? date('d.m.Y', strtotime($prediction->updated_at)) : false }}</dd>
        </dl>
    </div>

    <br>

    @if(isset($prediction->id))
        <form action="/admin/predictions/{{ $prediction->id }}" method="post">
            <input type="hidden" name="_method" value="DELETE">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" onclick="return confirm('Удалить?')" class="btn btn-default center-block"
                    style="border-color: red">Удалить прогноз?
            </button>
        </form>
    @endif

</div>


<br>
