<a href="/admin/predictions/create" class="btn btn-success">Добавить прогноз</a>

<h3 class="text-center">{{ $title }}</h3>

<div class="table-responsive">
    <table class="table table-bordered predictions table-hover">
        <tr style="background-color: #eee">
            <th>ID</th>
            <th>Дата</th>
            <th>Категория</th>
            <th>Место проведения</th>
            <th>Лига</th>
            <th>Команды</th>
            <th>Код</th>
            <th>Коэффициент</th>
            <th>Тип</th>
            <th>Результат</th>
            <th>Создан</th>
            <th>Обновлен</th>
        </tr>
        @foreach($predictions as $prediction)
            <tr onclick="location='/admin/predictions/{{ $prediction->id }}/edit'">
                <td class="text-center">{{ $prediction->id }}</td>
                <td class="text-center">{{ date('d.m.Y', strtotime($prediction->event_date)) }}</td>
                <td class="text-center">{{ $prediction->type_of_sport }}</td>
                <td class="text-center">{{ $prediction->region }}</td>
                <td class="text-center">{{ $prediction->league }}</td>
                <td class="text-center">{{ $prediction->team_one }} - {{ $prediction->team_two }}</td>
                <td class="text-center">{{ $prediction->code }}</td>
                <td class="text-center">{{ $prediction->rate }}</td>
                <td class="text-center">
                    @if($prediction->commercial)
                        <span class="glyphicon glyphicon-usd"></span>
                    @endif
                </td>
                <td class="text-center">
                    @if($prediction->result)
                        {{ $prediction->result }}
                    @endif
                </td>
                <td class="text-center">
                    @if($prediction->created_at)
                        {{ date('d.m.Y', strtotime($prediction->created_at)) }}
                    @endif
                </td>
                <td class="text-center">
                    @if($prediction->updated_at)
                        {{ date('d.m.Y', strtotime($prediction->updated_at)) }}
                    @endif
                </td>
{{--                <td><a href="/admin/predictions/{{ $prediction->id }}/edit"><i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i></a></td>--}}
            </tr>
        @endforeach
    </table>

    {{ $predictions->links() }}

</div>


