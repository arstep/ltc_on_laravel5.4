<h3 class="text-center">{{ $title }}: {{ $user->name }}</h3>

<form action="/admin/users/{{ $user->id }}" method="post">
    {{--<input type="hidden" name="_method" value="DELETE">--}}
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" onclick="return confirm('Удалить?')" class="btn btn-default btn-sm" style="border-color: red">
        Удалить пользователя
    </button>
</form>
<br>

<div class="row panel panel-default">
    <div class="col-sm-4">

        <h4 class="page-header">Общие данные</h4>

        <h3><span class="label label-warning">{{ $user->role }}</span></h3>
        <br>

        @if(isset($user->photo))
            <img class="img-responsive" width="100"
                 src="{{ $user->photo }}">
        @else
            <img class="img-responsive" width="100"
                 src="{{ asset(env('THEME')) }}/img/users/default_user.jpg">
        @endif

        <br>

        <form action="/admin/users/{{ $user->id }}" method="post">
            <div class="table-responsive">
                <table class="table table-bordered admin users">
                    <tr>
                        <td>ID</td>
                        <td>{{ $user->id }}</td>
                    </tr>
                    <tr>
                        <td>Права</td>
                        <td>
                            <select name="role" class="form-control">
                                @foreach($roles as $role)
                                    <option{{ ($role == $user->role) ? ' selected' : false }}>{{ $role }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ isset($user->email) ? $user->email : false }}</td>
                    </tr>
                    <tr>
                        <td>identity</td>
                        <td style="word-wrap: break-word">{{ isset($user->identity) ? $user->identity : false }}</td>
                    </tr>
                    <tr>
                        <td>network</td>
                        <td>{{ isset($user->network) ? $user->network : false }}</td>
                    </tr>
                    <tr>
                        <td>uid</td>
                        <td>{{ isset($user->uid) ? $user->uid : false }}</td>
                    </tr>
                    <tr>
                        <td>Создан</td>
                        <td>{{ date('d.m.Y', strtotime($user->created_at)) }}</td>
                    </tr>
                    <tr>
                        <td>Изменен</td>
                        <td>{{ date('d.m.Y', strtotime($user->updated_at)) }}</td>
                    </tr>
                </table>
            </div>

            {{ csrf_field() }}

            {{ method_field('PUT') }}

            <button type="submit" class="btn btn-success">Изменить</button>
        </form>

        <br>

    </div>

    <div class="col-sm-4">
        <h4 class="page-header">Информация о заказах</h4>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias earum facere illo laudantium neque rerum
            suscipit totam ut veniam! Accusamus ipsum nisi quos ratione sequi suscipit tempore vel velit
            voluptatibus.</p>
    </div>

    <div class="col-sm-4">
        <h4 class="page-header">Сообщения пользователя</h4>

        <br>

        <dl>
            @foreach($user->comments as $comment)
                <dt>Идентификатор статьи: {{ $comment->article_id }}</dt>
                <dt>Оценка: {{ $comment->score }}</dt>
                <dd>
                    <small>{{ date('d.m.Y', strtotime($user->created_at)) }}</small>
                </dd>
                <dd>{{ $comment->text }}</dd>
            @endforeach
        </dl>

    </div>
</div>

<br>




