@if($paginates)

                <h3 class="text-center">Список статей</h3>

                <div class="table-responsive">
                    <table class="table table-bordered admin">
                        <tr style="background-color: #eee">
                            <th>ID</th>
                            <th>Заголовок</th>
                            <th>Текст</th>
                            <th>Описание</th>
                            <th>Изображение</th>
                            <th>Добавил</th>
                            <th>Псевдоним</th>
                            <th>Действие</th>
                        </tr>

                        @foreach($paginates as $article)

                            <tr>
                                <td class="text-center">{{ $article->id }}</td>
                                <td class="text-left"><a
                                            {{--href="/admin/articles/edit/{{ $article->id }}">{{ $article->title }}</a>--}}
                                            href="/admin/articles/{{ $article->id }}/edit">{{ $article->title }}</a>
                                </td>
                                <td class="text-left">{!! str_limit($article->text, 300) !!}</td>
                                <td class="text-left">{!! str_limit($article->description, 100) !!}</td>
                                <td><img class="img-responsive"
                                         src="{{ asset(env('THEME')) }}/img/articles/{{ $article->img->mini }}">
                                </td>
                                <td class="text-center">{{ $article->user->name or 'none' }}</td>
                                <td class="text-center">{{ $article->alias }}</td>
                                <td class="text-center">
                                    {{--<form action="/admin/articles/destroy/{{ $article->id }}" method="post">--}}
                                    <form action="/admin/articles/{{ $article->id }}" method="post">
                                        {{--<input type="hidden" name="_method" value="DELETE">--}}
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" onclick="return confirm('Удалить?')" class="btn btn-danger btn-sm">Удалить</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>

                {{ $paginates->links() }}

                <br>

                <a href="/admin/articles/create" class="btn btn-success">Добавить статью</a>

                <br>
                <br>
@endif