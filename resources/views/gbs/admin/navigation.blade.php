@if($menu)

    <nav id="nav" class="admin">
        <ul>

            @foreach($menu as $key => $value)
                <li><a href="{!! $value !!}">{!! $key !!}</a></li>
            @endforeach

        </ul>
    </nav>

@endif