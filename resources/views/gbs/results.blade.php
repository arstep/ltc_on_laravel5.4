@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Результаты</span></li>
    </ul>

    <h3>Результаты прогнозов</h3>

    <br>

    <img class="img-responsive" src="{{ asset(env('THEME')) }}/img/results.jpg">

    <br>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid dicta distinctio eius est, porro repudiandae
        similique vitae. Culpa excepturi facere, fugiat id iste laboriosam minima quibusdam quis rem ullam,
        voluptas.</p>

    <hr>

    <div class="bs-example">
        <ul class="nav nav-pills nav-stacked" style="max-width: 300px;">
            <li><a href="#">Общая статистика</a></li>
            <li><a href="#">Результаты бесплатных прогнозов</a></li>
            <li><a href="#">Результаты платных прогнозов</a></li>
            <li><a href="#">Архив прогнозов</a></li>
        </ul>
    </div>

    <hr>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid dicta distinctio eius est, porro repudiandae
        similique vitae. Culpa excepturi facere, fugiat id iste laboriosam minima quibusdam quis rem ullam,
        voluptas.</p>

    <blockquote class="panel-warning">
        <p>
            Примечание
        </p>
        <footer>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid dicta distinctio eius est, porro repudiandae
            similique vitae. Culpa excepturi facere, fugiat id iste laboriosam minima quibusdam quis rem ullam,
            voluptas
        </footer>
    </blockquote>

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.subscribe')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.last_predictions')
    @include(env('THEME').'.sidebar.sendmail')
{{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
