@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Отзывы</span></li>
    </ul>

    <h3>Отзывы о прогнозах</h3>

    <br>

    <div class="alert alert-warning">
        <ul class="otzyv_rulez">
            <li>
                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>&nbsp;
                Мы публикуем все отзывы (как хорошие, так и плохие)
            </li>
            <li>
                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>&nbsp;
                У нас работает премодерация. Сообщения появляются после проверки
            </li>
            <li>
                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>&nbsp;
                Email не публикуется, он нужен для того, чтобы мы могли ответить на комментарий при
                необходимости
            </li>
            <li>
                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>&nbsp;
                Обязательными к заполнению являются только ваше имя и оценка. Отзыв — по желанию
            </li>
        </ul>
    </div>

{{--
@if(\Illuminate\Support\Facades\Auth::guest())
<script src="//ulogin.ru/js/ulogin.js"></script>

<div id="uLogin" data-ulogin="display=panel;theme=flat;fields=first_name,last_name,city,email;optional=photo;providers=vkontakte,odnoklassniki,facebook,twitter;redirect_uri=http%3A%2F%2Farstepfz.beget.tech%2Fsocial%2Fcallback_ulogin;mobilebuttons=0;"></div>
@endif
--}}


    {{--Подключение блока комментариев--}}
    @include(env('THEME').'.pages_include.comments')

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.subscribe')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.listsport')
    @include(env('THEME').'.sidebar.about_predictions')
    @include(env('THEME').'.sidebar.last_predictions')
    @include(env('THEME').'.sidebar.sendmail')
{{--    @include(env('THEME').'.sidebar.authentic')--}}
{{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
