@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/shop"></i>Купить</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>{{ $subscribe->title }}</span></li>
    </ul>

    <div class="row">
        <div class="col-sm-6">
            <h3>{{ $subscribe->title }}</h3>
            <br>
            <h3 id="price" class="label label-warning">{{ $subscribe->price }} &#8381;</h3>
            <br>
            <br>
            <br>

            {!! $subscribe->description !!}

            <hr>
            <br>

            <button class="btn btn-success btn-lg center-block" onclick="return Cart.add(event, {{ $subscribe->id }})">
                <strong>Подписаться!</strong></button>
            &nbsp;
        </div>
        <div class="col-sm-6 desc_goods panel panel-default">
            <img src="{{ asset(env('THEME')) }}/img/LTC.png" class="img-responsive">

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias asperiores deleniti
                doloremque earum enim hic illum impedit iste magni maxime non numquam perferendis perspiciatis
                recusandae repellendus, vero, voluptate voluptatibus!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias asperiores deleniti
                doloremque earum enim hic illum impedit iste magni maxime non numquam perferendis perspiciatis
                recusandae repellendus, vero, voluptate voluptatibus!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias asperiores deleniti
                doloremque earum enim hic illum impedit iste magni maxime non numquam perferendis perspiciatis
                recusandae repellendus, vero, voluptate voluptatibus!</p>
        </div>
    </div>




    {{--Вывод сообщений по работе с AJAX--}}
    <div id="modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.sendmail')
    {{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
