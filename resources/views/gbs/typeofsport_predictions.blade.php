@extends('gbs.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/predictions">Прогнозы на спорт</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active">
            <span>
                Прогнозы на {{ $typeOfSport->name_ru }}
            </span>
        </li>
    </ul>

    <br>

    <img src="{{ asset(env('THEME')) }}/img/middle_{{ $typeOfSport->name_en }}.png" width="200">

    <br>
    <br>

    <h4>Прогнозы на {{ $typeOfSport->name_ru }}</h4>

    <div class="table_with_pagination"  data-sport="{{ $typeOfSport->name_en }}">

        {{--Используем ту же форму что и для ajaxa--}}
        @include(env('THEME') . '.pages_include.ajax_table_with_pagination')

    </div>


    {{--Подключение HTML описания прогнозов по данному виду спорта--}}
    @include(env('THEME').'.pages_include.'. $typeOfSport->name_en)

@endsection

@section('sidebar')
    @include('gbs.sidebar.subscribe')
    @include('gbs.sidebar.cart')
    @include('gbs.sidebar.dispath')
    @include('gbs.sidebar.listsport')
    @include('gbs.sidebar.last_predictions')
{{--    @include('gbs.sidebar.search')--}}
@endsection
