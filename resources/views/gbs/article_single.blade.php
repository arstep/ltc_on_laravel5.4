@extends('gbs.layouts.rightsidebar')


@section('content')

    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/articles">Статьи</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active">
            <span>{{ $article->title }}</span>
        </li>
    </ul>

    <br>

    <img class="img-responsive" src="{{ asset(env('THEME')) }}/img/articles/{{ $article->img->max }}">

    <br>

    <div class="article">
        <h3>{{ $article->title }}</h3>

        {!! $article->text !!}
    </div>

    <br>

@endsection

@section('sidebar')
    @include(env('THEME').'.sidebar.subscribe')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.last_predictions')
    @include(env('THEME').'.sidebar.sendmail')
{{--    @include('gbs.sidebar.search')--}}
@endsection
