@extends('gbs.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Статьи</span></li>
    </ul>

    <h3>Статьи о прогнозировании вообще и прогнозах</h3>

    @foreach($paginates as $article)
        <hr>
        <div class="row articles-list">
            <div class="col-sm-3">

                <img class="img-responsive" src="{{ asset(env('THEME')) }}/img/articles/{{ $article->img->mini }}">
            </div>
            <div class="col-sm-9">
                <h4>{{ $article->title }}</h4>
                {!! $article->description !!}
                <a href="/article/{{ $article->id }}" class="btn btn-default">Читать далее</a>
            </div>
        </div>
    @endforeach

    <br>
    <br>

    {{ $paginates->links() }}
@endsection

@section('sidebar')
    @include(env('THEME').'.sidebar.subscribe')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.last_predictions')
    @include(env('THEME').'.sidebar.sendmail')
{{--    @include('gbs.sidebar.search')--}}
@endsection
