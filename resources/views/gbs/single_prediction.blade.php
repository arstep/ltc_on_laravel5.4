@extends('gbs.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/predictions">Прогнозы на спорт</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="{{ '/predictions/'.$typeOfSport->name_en }}">Прогнозы на {{ $typeOfSport->name_ru }}</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active">
            <span>
                {{ $prediction->commercial ? 'Платный' : 'Бесплатный' }} спортпрогноз на {{ date('d.m.Y', strtotime($prediction->event_date)) }}
            </span>
        </li>
    </ul>

    <br>

    <img class="img-responsive" src="{{ asset(env('THEME')) }}/img/vs.png">

    <br>

    <br>

    <h4>{{ $prediction->commercial ? 'Платный' : 'Бесплатный' }} спортпрогноз на {{ date('d.m.Y', strtotime($prediction->event_date)) }}</h4>

    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td>{{ date('d.m.Y', strtotime($prediction->event_date)) }}</td>
                <td>{{ mb_convert_case($typeOfSport->name_ru, MB_CASE_TITLE, "UTF-8") }}</td>
                <td>{{ $prediction->region }}</td>
                <td>{{ $prediction->league }}</td>
                <td>{{ $prediction->team_one }} - {{ $prediction->team_two }}</td>
                <td>{{ $prediction->code }}</td>
                <td>{{ $prediction->rate }}</td>
            </tr>
        </table>
    </div>

    <br>
    <br>
@endsection

@section('sidebar')
    @include('gbs.sidebar.subscribe')
    @include('gbs.sidebar.cart')
    @include('gbs.sidebar.dispath')
    @include('gbs.sidebar.listsport')
    @include('gbs.sidebar.last_predictions')
{{--    @include('gbs.sidebar.search')--}}
@endsection
