@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Купить</span></li>
    </ul>

    <h3>Подписка на платные прогнозы</h3>

    <br>

    <p><strong>Платные прогнозы</strong> — это хороший инструмент инвестирования. Прогнозы на спорт, выдаваемые
        алгоритмом LTC отличаются высокой стабильностью выдаваемых результатов, что может гарантировать получение
        реальной прибыли игрокам.</p>

    <div class="alert alert-success">
        <p>Высокие показатели стабильности платных прогнозов LTC прекрасно сочетаются с фактическими показателями
            заходимости. Это значит, что купив платный прогноз, игрок в одночасье не получит сверх-высокую прибыль. Но
            сможет без труда создать прибыльную модель игры, опираясь на ту самую стабильность результатов.</p>
        <p>Понимая это, мы создали серию подписок на получение платных прогнозов, отличающихся одними из самых
            демократичных цен на рынке. Цена одного прогноза без скидок начинается от 41 рубля. А ведь мы часто проводим
            акции, когда прогнозы можно купить еще дешевле!</p>
    </div>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto eos eveniet facilis incidunt libero minus
        non perferendis qui temporibus ullam! Alias aperiam commodi quia sunt? Accusantium beatae dolorem iste
        laboriosam.</p>

    <hr>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto eos eveniet facilis.</p>

    <p>Architecto eos eveniet facilis incidunt libero minus non perferendis qui temporibus ullam! Alias aperiam commodi
        quia sunt? Accusantium beatae dolorem iste laboriosam.</p>

    <br>


    <div class="row shop panel-body">
        <div class="row">

        @foreach($arrSubscribes as $blockSubscribes)

            <div class="col-lg-6">
                <div class="row">

                @foreach($blockSubscribes as $subscribe)

                        <div class="col-sm-6 subscribe">
                            <div>
                                <h4><a href="/shop/subscribe/{{ $subscribe->id }}">{!! $subscribe->title !!}</a></h4>
                                <img src="{{ asset(env('THEME')) }}/img/LTC.png" class="img-responsive">
                                <a href="#" class="butt addtocart" onclick="return Cart.add(event, {{ $subscribe->id }})">
                                    <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                                    <span>{{ $subscribe->price }}</span>
                                    &nbsp;&#8381;
                                </a>
                            </div>
                        </div>

                @endforeach

                </div>
            </div>

        @endforeach

        </div>
    </div>


    {{--Вывод сообщений по работе с AJAX--}}
    <div id="modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.cart')
    @include(env('THEME').'.sidebar.dispath')
    @include(env('THEME').'.sidebar.sendmail')
{{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
