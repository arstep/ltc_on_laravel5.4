@extends('gbs.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active">
            <span>Прогнозы на спорт</span>
        </li>
    </ul>

    <br>

    <h3>Последние прогнозы по видам спорта</h3>


    @foreach($arrPaginates as $typeOfSport => $paginates)

        <hr>
        <a href="/predictions/{{ $typeOfSport }}">

            <img src="{{ asset(env('THEME')) }}/img/symbol_{{ $typeOfSport }}.jpg">

            <h4>
                Прогноз на {{ \Illuminate\Support\Facades\Lang::get('gbs.'.$typeOfSport) }}
                <small>Перейти в рубрику</small>

            </h4>
        </a>


        <div class="table_with_pagination" data-sport="{{ $typeOfSport }}">

            {{--Используем ту же форму что и для ajaxa--}}
            @include(env('THEME') . '.pages_include.ajax_table_with_pagination')

        </div>


        <br>

    @endforeach

@endsection

@section('sidebar')
    @include('gbs.sidebar.subscribe')
    @include('gbs.sidebar.cart')
    @include('gbs.sidebar.dispath')
    @include('gbs.sidebar.last_predictions')
    @include(env('THEME').'.sidebar.about_predictions')
{{--    @include('gbs.sidebar.search')--}}
@endsection
