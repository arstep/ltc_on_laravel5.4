@if($name == 'name')
    <form action="/cabinet/userData" method="post" class="form-horizontal" role="form">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Внесите изменения</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-4 control-label">Имя</label>
                        <div class="col-sm-4">
                            <input name="name" type="text" class="form-control" id="name" placeholder="Имя">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Изменить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

        {{ csrf_field() }}
    </form>

@elseif($name == 'email')

    <form action="/cabinet/userData" method="post" class="form-horizontal" role="form">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Внесите изменения</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-4">
                            <input name="email" type="email" class="form-control" id="email" placeholder="email">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Изменить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

        {{ csrf_field() }}
    </form>

@elseif($name == 'password')

    <form action="/cabinet/userData" method="post" class="form-horizontal" role="form">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Внесите изменения</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Пароль</label>
                        <div class="col-sm-4">
                            <input name="password" type="password" class="form-control" id="password" placeholder="6 символов">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-sm-4 control-label">Подтверждение</label>
                        <div class="col-sm-4">
                            <input name="password_confirmation" type="password" class="form-control" id="password_confirmation" placeholder="6 символов">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Изменить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

        {{ csrf_field() }}
    </form>

@else

    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <strong>Неправильные данные</strong>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

@endif