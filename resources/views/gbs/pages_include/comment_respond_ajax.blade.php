<li id="li-comment-{{ $data['id'] }}" class="media">
    <div  class="media-left">
        <a href="#">
            <img src="{{ asset(env('THEME')) }}/img/default_user.jpg" style="width: 40px">
        </a>
    </div>
    <div id="comment-{{ $data['id'] }}" class="media-body">
        <div class="media-heading">
            <div class="author">{{ $data['name'] }}</div>
            <div class="metadata">
                    <span class="date">

                    </span>
                <span class="devide">
                        |
                    </span>
                <span class="comment_number">
                        #{{ $data['id'] }}
                    </span>
            </div>
        </div>
        <div class="media-text text-justify comment-text-{{ $data['id'] }}">
            {{ $data['text'] }}
        </div>

        <div class="footer-comment">
                <span class="rating">
                  Оценка: {{ $data['score'] }}
                </span>
            <span class="devide">
                  |
                </span>
            <span class="comment-reply">
                  <a href="#respond" class="reply" onclick="return getForm.move(event, '{{ $data['id'] }}')">ответить</a>
                </span>
        </div>
    </div>
</li>