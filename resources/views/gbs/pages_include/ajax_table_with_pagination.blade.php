<div class="table-responsive">
    <table class="table table-bordered">

        @foreach($paginates as $paginate)
            <tr>
                <td style="vertical-align: middle;">{{ date('d.m.Y', strtotime($paginate->event_date)) }}</td>
                <td style="vertical-align: middle;">{{ $paginate->region }} ({{ $paginate->league }})</td>
                <td style="vertical-align: middle;">{{ $paginate->team_one }} - {{ $paginate->team_two }}</td>
                <td style="vertical-align: middle;">
                    <a href="">
                        <a href="{{ route('prediction', ['typeOfSport' => $paginate->type_of_sport, 'idPrediction' => $paginate->id]) }}"
                           class="btn btn-primary btn-sm">Перейти</a>
                    </a>
                </td>
            </tr>
        @endforeach

    </table>
</div>

{{ $paginates->links() }}