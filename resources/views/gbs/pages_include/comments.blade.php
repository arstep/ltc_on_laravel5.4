<br>

<div class="comments">
    <h3 class="title-comments">{{ $titleComments or 'Комментарии' }} ({{ $comments_count }})</h3>

    @if($comments_count > 0)

        <ul class="media-list">
            @include(env('THEME').'.pages_include.comment_single', ['items' => $parent])
        </ul>
    @endif
</div>


<br>

{{--Вывод сообщения о результате AJAX отправки формы--}}
<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="formcomment">

    <div id="respond" class="well sidebar-nav">

        <form id="form-send-comment" action="/comment" method="post">
            <h4>Ваш отзыв</h4>

            {{--Если пользователь не аутентифицирован - показываем поля--}}
            @if( ! \Illuminate\Support\Facades\Auth::check())
                <div class="form-group">
                    <label for="name">Ваше имя</label>
                    <input name="name" type="text" class="form-control" id="name" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="email">Ваш Email</label>
                    <input name="email" type="email" class="form-control" id="email" placeholder="email">
                </div>
            @endif

            <p>Поставьте оценку</p>
            <label class="radio-inline">
                <input type="radio" name="score" value="1"> 1
            </label>
            <label class="radio-inline">
                <input type="radio" name="score" value="2"> 2
            </label>
            <label class="radio-inline">
                <input type="radio" name="score" value="3"> 3
            </label>
            <label class="radio-inline">
                <input type="radio" name="score" value="4"> 4
            </label>
            <label class="radio-inline">
                <input type="radio" name="score" value="5"> 5
            </label>

            <br>

            <div class="form-group">
                <label for="text">Комментарий</label>
                <textarea name="text" id="text" class="form-control" rows="3"></textarea>
            </div>

            {{--Идентификатор статьи--}}
            <input name="comment_article_id" id="comment_article_id" type="hidden" value="0">

            {{--Идентификатор родительского комментария--}}
            <input name="comment_parent_id" id="comment_parent_id" type="hidden" value="0">

            {{ csrf_field() }}

            <button id="submit" type="submit" class="btn btn-default">Отправить</button>
        </form>

    </div><!--/.well -->

</div>