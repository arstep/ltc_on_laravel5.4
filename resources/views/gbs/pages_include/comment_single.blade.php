@foreach($items as $item)

    <li id="li-comment-{{ $item->id }}" class="media">
        <div  class="media-left">
            <a href="#">
                <img src="{{ asset(env('THEME')) }}/img/default_user.jpg" style="width: 40px">
            </a>
        </div>
        <div id="comment-{{ $item->id }}" class="media-body">
            <div class="media-heading">
                <div class="author">{{ ($item->name) ? $item->name : $item->user->name}}</div>
                <div class="metadata">
                    <span class="date">
                        {{ is_object($item->created_at) ? $item->created_at->format('d.m.Y в H:i') : '' }}
                    </span>
                    <span class="devide">
                        |
                    </span>
                    <span class="comment_number">
                        #{{ $item->id }}
                    </span>
                </div>
            </div>
            <div class="media-text text-justify comment-text-{{ $item->id }}">
                {{ $item->text }}
            </div>

            <div class="footer-comment">
                <span class="rating">
                  Оценка: {{ $item->score }}
                </span>
                <span class="devide">
                  |
                </span>
                <span class="comment-reply">
                  <a href="#respond" class="reply" onclick="return getForm.move(event, '{{ $item->id }}')">ответить</a>
                </span>
            </div>
        </div>


        {{--Если в массиве есть ключ с номером этого коммента - значит там находятся ответы на этот коммент--}}
        @if(isset($reply[$item->id]))
            <ul>
                {{--Рекурсивный вывод дочерних комментов--}}
                @include(env('THEME').'.pages_include.comment_single', ['items' => $reply[$item->id]])
            </ul>
        @endif

    </li>

@endforeach
