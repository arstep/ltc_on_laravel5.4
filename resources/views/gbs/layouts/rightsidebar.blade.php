@include('gbs.layouts.header')

    <div class="main container">
        <div class="row">

            <!--Main-->
            <div class="col-sm-7 col-md-8 col-lg-9">

                 @yield('content')

            </div>
            <!--Main-->

            <!--Sidebar-->
            <div class="col-sm-5 col-md-4 col-lg-3">

                @section('sidebar')
                @show

            </div>
            <!--Sidebar-->

        </div>
    </div>

@include('gbs.layouts.footer')