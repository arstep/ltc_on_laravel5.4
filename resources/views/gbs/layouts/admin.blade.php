<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>{{ $title or 'Административный раздел' }}</title>

    <link rel="shortcut icon" href="{{ asset(env('THEME')) }}/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/bootstrap.css"/>
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/style.css"/>
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/font-awesome.min.css">

    <!-- Scripts -->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/jquery.dropotron.min.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/skel.min.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/skel-viewport.min.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/util.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/main.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/bootstrap.min.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/to_top.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/comment_reply.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/comment_ajax.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/cart_ajax.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/ajax_table_prediction.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/my_functions.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/ckeditor/ckeditor.js"></script>
    <script src="{{ asset(env('THEME')) }}/js/bootstrap-filestyle.js"></script>

</head>
<body>

<div id="page-wrapper">


    <div class="container top">

        <div class="row">
            <div class="col-sm-7"><h1 id="logotype"><a href="/">Logotype Title Com</a></h1></div>
            <div class="col-sm-5 auth_site">
                <ul id="auth_site">
                    <!-- Authentication Links -->
                    @if (Auth::guest())

                        <li><a href="{{ route('login') }}"> <i class="fa fa-sign-in"></i> Войти</a></li>
                        <li><a href="{{ route('register') }}">&nbsp; <i class="fa fa-user"></i> Зарегистрироваться</a>
                        </li>
                    @else

                        <li class="dropdown">
                            <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="/cabinet/main"><i class="fa fa-user"></i> Личный кабинет</a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> Выйти
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>



{{--Подключение секции навигация--}}
@yield('navigation')


    <div class="main container admin">
        <div class="row">
            <!--Main-->
            <div class="col-sm-12">


            {{--Секция вывода ошибок--}}


                @if(count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif


                @if(session('status'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ session('status') }}</p>
                    </div>
                @endif


                @if(session('error'))
                    <div class="alert alert-danger" role="alert">
                        <p>{{ session('error') }}</p>
                    </div>
                @endif


{{--Подключение контента--}}
@yield('content')
                {{--{!! $content !!}--}}

            </div>
            <!--Main-->
        </div>
    </div>



@yield('footer')