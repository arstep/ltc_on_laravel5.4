<footer class="bottom">
    <div class="container"><br>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <h4>
                        <i class="glyphicon glyphicon-stats" aria-hidden="true" style="color: #e3e6d2;"></i>
                        &nbsp;&nbsp;Прогнозы на спорт</h4>

                    @foreach($listSport as $item)
                        <p><a href="/predictions/{{ $item->name_en }}">Прогнозы на {{ $item->name_ru }}</a></p>
                    @endforeach

                </div>
                <div class="col-sm-3">
                    <h4>
                        <i class="glyphicon glyphicon-home" aria-hidden="true" style="color: #e3e6d2;"></i>
                        &nbsp;&nbsp;О компании</h4>
                    <p><a href="/about">Об алгоритме LTC</a></p>
                    <p><a href="/reviews">Отзывы</a></p>
                </div>
                <div class="col-sm-3">
                    <h4>
                        <i class="glyphicon glyphicon-paperclip" aria-hidden="true" style="color: #e3e6d2;"></i>
                        &nbsp;&nbsp;Пресс-центр</h4>
                    <p><a href="#">Партнерская программа</a></p>
                </div>
                <div class="col-sm-3">
                    <h4>
                        <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true" style="color: #e3e6d2;"></i>
                        &nbsp;&nbsp;Об оплате</h4>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/qiwi.png" height="30" width="50"/></a>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/yandexmoney.png" height="30" width="50"/></a>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/webmoney-blue.png" height="30" width="50"/></a>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/paypal.png" height="30" width="50"/></a>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/visa.png" height="30" width="50"/></a>
                    <a class="pay" href="/"><img src="{{ asset(env('THEME')) }}/img/mastercard.png" height="30" width="50"/></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<a id="toTop" class="btn btn-default" href="/" role="button">
    <i class="glyphicon glyphicon-chevron-up" aria-hidden="true"></i>
    Наверх
</a>


{{--Размещаем проверку сообщения в сессии для вывода после загрузки страницы (модальное окно в my_functions.js)--}}
@if(session()->has('show_modal_msg'))

    <script> window.onload = function () {
            var msg = "<h4 class='text-success text-center'>{{ session()->get('show_modal_msg') }}</h4>";
            show_modal_msg(msg);
        }</script>
@endif


<!-- Scripts -->
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="{{ asset(env('THEME')) }}/js/jquery.dropotron.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/skel.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/skel-viewport.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/util.js"></script>
<script src="{{ asset(env('THEME')) }}/js/main.js"></script>
<script src="{{ asset(env('THEME')) }}/js/bootstrap.min.js"></script>
<script src="{{ asset(env('THEME')) }}/js/to_top.js"></script>
<script src="{{ asset(env('THEME')) }}/js/comment_reply.js"></script>
<script src="{{ asset(env('THEME')) }}/js/comment_ajax.js"></script>
<script src="{{ asset(env('THEME')) }}/js/cart_ajax.js"></script>
<script src="{{ asset(env('THEME')) }}/js/ajax_table_prediction.js"></script>
<script src="{{ asset(env('THEME')) }}/js/my_functions.js"></script>
</div>
</body>
</html>