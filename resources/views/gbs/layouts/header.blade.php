<!DOCTYPE HTML>
<html>
<head>
    <title>{{ $titleHead or 'Спортпрогнозы' }}</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="shortcut icon" href="{{ asset(env('THEME')) }}/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/bootstrap.css"/>
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/style.css"/>
    <link rel="stylesheet" href="{{ asset(env('THEME')) }}/css/font-awesome.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div id="page-wrapper">


    <div class="container top">

        <div class="row">
            <div class="col-sm-7"><h1 id="logotype"><a href="/">Logotype Title Com</a></h1></div>
            <div class="col-sm-5 auth_site">
                <ul id="auth_site">
                    <!-- Authentication Links -->
                    @if (Auth::guest())

                        <li><a href="{{ route('login') }}"> <i class="fa fa-sign-in"></i> Войти</a></li>
                        <li><a href="{{ route('register') }}">&nbsp; <i class="fa fa-user"></i> Зарегистрироваться</a>
                        </li>
                    @else

                        <li class="dropdown">
                            <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="/cabinet/main"><i class="fa fa-user"></i> Личный кабинет</a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> Выйти
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>


    </div>
{{--{{ dd($typesSport) }}--}}

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li class="current"><a href="/">Главная</a></li>
            <li><a href="/about">Описание</a></li>
            <li><a href="/articles">Статьи</a></li>
            <li>
                <a href="/predictions">Спортпрогнозы<span class="caret"></span> </a>
                <ul>
                    @foreach($listSport as $item)
                        <li><a href="/predictions/{{ $item->name_en }}">Прогнозы на {{ $item->name_ru }}</a></li>
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="/results">Результаты<span class="caret"></span> </a>
                <ul>
                    <li><a href="#">Общая статистика</a></li>
                    <li><a href="#">Бесплатные прогнозы</a></li>
                    <li><a href="#">Платные прогнозы</a></li>
                    <li>
                        <hr>
                    </li>
                    <li><a href="#">Архив прогнозов</a></li>
                </ul>
            </li>
            <li><a href="/reviews">Отзывы</a></li>
            <li><a href="/shop">Купить</a></li>
        </ul>
    </nav>
    <!-- Nav -->


