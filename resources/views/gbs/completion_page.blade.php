@extends(env('THEME').'.layouts.rightsidebar')


@section('content')
    <ul class="crumbs">
        <li>
            <a href="{{ route('index') }}"><i class="glyphicon glyphicon-home" aria-hidden="true"></i></a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/shop">Купить</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li>
            <a href="/cart">Корзина</a>
            <i class="glyphicon glyphicon-menu-right" aria-hidden="true" style="font-size: 0.7em"></i>
        </li>
        <li class="active"><span>Оформление заказа</span></li>
    </ul>

    <h3>Оформление заказа</h3>

    <br>


    <div class="col-lg-8 col-lg-offset-2">
        <dl class="well dl-horizontal">
            <dt>Наименование</dt>
            <dd>{{ $goods['title'] }}</dd>
            <br>
            <dt>Ваше имя</dt>
            <dd>{{ $user->name }}</dd>
            <dt>Email</dt>
            <dd>{{ $user->email }}</dd>
        </dl>

        <h4 class="text-right"><strong>ИТОГО: {{ $goods['price'] }} &#8381;</strong></h4>

        <a href="#" class="btn btn-primary" onclick="show_modal_msg('<h4><strong>Тест. Модуль оплаты отсутствует.</strong></h4>'); return false">Оплатить</a>

        <br>
        <br>
        <br>
    </div>

@endsection


@section('sidebar')
    @include(env('THEME').'.sidebar.sendmail')
    @include(env('THEME').'.sidebar.dispath')
    {{--    @include(env('THEME').'.sidebar.search')--}}
@endsection
